<!--
  Copyright (c) 2024 Eclipse Foundation, Inc.

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License v. 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
-->
<div>
  <p>
    <strong>Please see the <a href="/org/documents/epl-2.0/faq.php">Eclipse Public License 2.0
        FAQ</a>.
    </strong>
  </p>
  <p>
    <strong>For informational purposes only.</strong>
  </p>
  <p>
    This FAQ attempts to provide answers to commonly asked questions related to
    <em>the <a href="/org/documents/epl-v10.php">Eclipse Public License 1.0
        (EPL)</a></em>. It is provided for informational purposes only. It is
    not part of, nor does it modify, amend, or supplement the terms of the EPL.
    The EPL is a legal agreement that governs the rights granted to material
    licensed under it, so please read it carefully. If there is any conflict
    between this FAQ and the EPL, the terms of the EPL shall govern. This FAQ
    should not be regarded as legal advice. If you need legal advice, you must
    contact your own lawyer.
  </p>
  <div class="homeitem3col">
    <h3>Table of Contents</h3>
    <ol>
      <li><a href="#CPLEPL">What is the relationship between the CPL and the EPL
          1.0?</a></li>
      <li><a href="#WHYEPL">Why was the EPL 1.0 written?</a></li>
      <li><a href="#EPLDIFFER">Specifically how does the EPL 1.0 differ from the
          CPL?</a></li>
      <li><a href="#EPLVERSION">What is the latest version of the EPL</a></li>
      <li><a href="#MEMAPPROVE">Do all Eclipse Foundation members approve of the
          EPL?</a></li>
      <li><a href="#TRANSITION">How and when will the Eclipse Foundation
          transition from the CPL to the EPL 1.0?</a></li>
      <li><a href="#EPLOSI">Is the EPL approved by the Open Source Initiative
          (OSI)?</a></li>
      <li><a href="#LICAPP">What is required for OSI license approval?</a></li>
      <li><a href="#BUSADVOS">What are the business advantages of the Open
          Source model?</a></li>
      <li><a href="#TECHADVOS">What are the technical advantages of the Open
          Source model?</a></li>
      <li><a href="#PARTIESEPL">How are the parties defined in the EPL 1.0?</a></li>
      <li><a href="#ANONCONTR">Can a Contributor remain anonymous?</a></li>
      <li><a href="#RECRIGHTS">What rights do Contributors grant Recipients
          under EPL 1.0?</a></li>
      <li><a href="#USEINANOTHER">Does the EPL 1.0 allow me to take the Source
          Code for a Program licensed under it and include all or part of it in
          another program licensed under the GPL, BSD license or other Open
          Source license?</a></li>
      <li><a href="#COMPILEWOMOD">Can I take a Program licensed under the EPL
          1.0, compile it without modification, and commercially license the
          result?</a></li>
      <li><a href="#SOURCEWOBJ">Do I need to include the source code for such
          Program with the object code distribution?</a></li>
      <li><a href="#PROPPROD">When I incorporate a portion of a Program licensed
          under the EPL 1.0 into my own proprietary product distributed in
          object code form, can I use a single license for the full product, in
          other words, covering the portion of the Program plus my own code?</a></li>
      <li><a href="#AGREESTEWARD">The EPL states that it can be changed by the
          Agreement Steward. Does a Contributor have the choice of
          redistributing a previously distributed Program under the old or the
          new version of the EPL?</a></li>
      <li><a href="#MODNODIST">If I modify a Program licensed under the EPL, but
          never distribute it to anyone else, do I have to make my modifications
          available to others?</a></li>
      <li><a href="#MODDIST">If I modify a Program licensed under the EPL and
          distribute the object code of the modified Program for free, must I
          make the source code available?</a></li>
      <li><a href="#MODULEDIST">If I write a module to add to a Program licensed
          under the EPL and distribute the object code of the module along with
          the rest of the Program, must I make the source code to my module
          available in accordance with the terms of the EPL?</a></li>
      <li><a href="#SRCREDIST">What are my obligations if I copy source code
          obtained from Eclipse.org and licensed under the Eclipse Public
          License and include it in my product that I then distribute?</a></li>
      <li><a href="#EPLWARRANTY">Does the EPL offer any warranty with regard to
          the Program?</a></li>
      <li><a href="#GETANSWER">This document does not have the answer to my
          question. How can I get my question answered?</a></li>
      <li><a href="#DERIV">Some open source software communities specify what
          they mean by a &quot;derivative work&quot;. Does the Eclipse
          Foundation have a position on this?</a></li>
      <li><a href="#LINK">Some free software communities say that linking to
          their code automatically means that your program is a derivative work.
          Is this the position of the Eclipse Foundation?</a></li>
      <li><a href="#EXAMPLE">I&lsquo;m a programmer not a lawyer, can you give
          me a clear cut example of when something is or is not a derivative
          work?</a></li>
      <li><a href="#USEEPL">I am starting my own open source software project.
          Does the Eclipse Foundation allow me to use the EPL for my project?</a></li>
      <li><a href="#CODEGEN">Many Eclipse tools and wizards use code templates
          which are included in the application that is generated. Is the code
          generated by these tools considered a derivative work that must be
          licensed under the EPL?</a></li>
      <li><a href="#3RDPARTY">What licenses are acceptable for third-party code
          redistributed by Eclipse projects?</a></li>
      <li><a href="#OSICOMPLIANT">Is an OSI-compliant license a requirement for
          all third-party code redistributed by Eclipse projects?</a></li>
      <li><a href="#GPLCOMPATIBLE">Are the Eclipse Public License (EPL) 1.0 and
          the General Public License (GPL) compatible?</a></li>
      <li><a href="#DUALLIC">For dual-licensed projects, which license terms
          apply?</a></li>
    </ol>
  </div>
  <h3>Frequently Asked Questions</h3>
  </blockquote>
  <ol>
    <li><strong><a name="CPLEPL">What is the relationship between IBM&rsquo;s
          Common Public License (CPL) and the Eclipse Public License 1.0 (EPL)?</a></strong><br />
      </b>The Eclipse SDK codebase was originally distributed under the CPL. The
      EPL 1.0 was derived from <a href="http://www-128.ibm.com/developerworks/library/os-cpl.html" target="_blank">CPL
        version 1.0</a>. As a result, much of the information provided in the <a
        href="http://www.ibm.com/developerworks/opensource/library/os-cplfaq/index.html" target="_blank">Common Public
        License (CPL) Frequently Asked Questions</a> document is
      relevant to the EPL, as well. The purpose of this FAQ is to highlight the
      differences.<br /> <br /></li>
    <li><strong><a name="WHYEPL">Why was the EPL 1.0 written?</a></strong><br />
      The EPL 1.0 was written specifically for the <a href="/org/" target="_top">Eclipse Foundation</a>. First, it
      changes the Agreement Steward, formerly
      IBM for the CPL, to now be the Eclipse Foundation for the EPL. Second, it
      addresses concerns some Eclipse Foundation members had with how the CPL
      deals with possible patent litigation.<br /> <br /></li>
    <li><strong><a name="EPLDIFFER">Specifically how does the EPL 1.0 differ
          from the CPL?</a><br /> </strong>Section 7 of the CPL contained the
      following language:<br /> <br /> <em>&quot;If Recipient institutes patent
        litigation against a Contributor with respect to a patent applicable to
        software (including a cross-claim or counterclaim in a lawsuit), then
        any patent licenses granted by that Contributor to such Recipient under
        this Agreement shall terminate as of the date such litigation is filed.
        In addition, if Recipient institutes patent litigation against any
        entity (including a cross-claim or counterclaim in a lawsuit) alleging
        that the Program itself (excluding combinations of the Program with
        other software or hardware) infringes such Recipient&rsquo;s patent(s),
        then such Recipient&rsquo;s rights granted under Section 2(b) shall
        terminate as of the date such litigation is filed.&quot;<br /> <br />
      </em> The first sentence was removed in the EPL 1.0. Many members and
      prospective members believed that the first sentence was overly broad and
      viewed it as an inhibitor to the continued growth of the Eclipse
      eco-system. The second sentence remains unchanged in the EPL 1.0.<br /> <br />
      The current <a href="/org/documents/Eclipse_IP_Policy.pdf">Eclipse
        Foundation Intellectual Property Policy</a> further clarifies the
      general principles under which the Eclipse Foundation shall accept
      contributions, license contributions, license materials owned by the
      Eclipse Foundation, and manage other intellectual property matters.<br />
      <br />
    </li>
    <li><strong><a name="EPLVERSION">What is the latest version of the EPL?</a><br />
      </strong><a href="epl-2.0">Version 2.0</a> is the latest version of the EPL.
      Please see the <a href="/org/documents/epl-2.0/faq.php">Eclipse Public License 2.0
        FAQ</a>.<br /> <br /></li>
    <li><strong><a name="MEMAPPROVE">Do all Eclipse Foundation members approve
          of the EPL?</a><br /> </strong>Yes, the Eclipse Foundation membership
      approved the EPL 1.0 unanimously. Future members must agree to abide by
      the EPL and the Intellectual Property Policy as part of joining the
      Eclipse Foundation and signing the <a
        href="/org/documents/Eclipse%20MEMBERSHIP%20AGMT%202003_11_10%20Final.pdf">Eclipse Foundation Membership
        Agreement</a>.<br /> <br /></li>
    <li><strong><a name="TRANSITION">How and when will the Eclipse Foundation
          transition from the CPL to the EPL 1.0?</a><br /> </strong>For
      details, see the <a href="/legal/cpl2epl/cpl2epl-transition-plan.pdf">CPL to EPL
        Transition Plan</a> (.pdf) and the <a href="/legal/cpl2epl/faq">CPL
        to EPL Transition Plan FAQ</a>.<br /> <br /></li>
    <li><strong><a name="EPLOSI">Is the EPL approved by the Open Source
          Initiative (OSI)?</a><br /></strong> Yes, the EPL has been approved.
      Version 1.0 was approved in May 2004. See the complete <a href="http://opensource.org/licenses/"
        target="_blank">list of OSI-approved licenses</a>.<br /> <br /></li>
    <li><strong><a name="LICAPP">What is required for OSI license approval?</a><br /></strong>
      A license qualifies for OSI approval if it conforms to the OSI&rsquo;s
      &quot;Open Source Definition&quot; or &quot;OSD.&quot; The OSD covers nine
      topics of concern. Chief among these is the requirement that a license not
      restrict any party from selling or giving away the software. Further, the
      Program must include source code, must allow distribution in source code
      as well as compiled form, and must allow modifications and derived works.
      Find more information on the <a href="http://www.opensource.org/docs/definition.php" target="_blank">OSD</a> at
      opensource.org.<br /> <br /></li>
    <li><strong><a name="BUSADVOS">What are the business advantages of the Open
          Source model?<br /></strong> </a>An Open Source community provides a
      way for individuals and companies to collaborate on projects that would be
      difficult to achieve on their own..<br /> <br /></li>
    <li><strong><a name="TECHADVOS">What are the technical advantages of the
          Open Source model?</strong></a><br /> The Open Source model has the
      technical advantage of turning users into potential co-developers. With
      source code readily available, users will help you debug quickly and
      promote rapid code enhancements. &quot;Given a bit of encouragement, your
      users will diagnose problems, suggest fixes, and help improve the code far
      more quickly than you could unaided.&quot; (The Cathedral and the Bazaar,
      Eric Steven Raymond. See <a href="http://tuxedo.org/~esr/writings/cathedral-bazaar/"
        target="_blank">http://tuxedo.org/~esr/writings/cathedral-bazaar/</a>)<br /> <br /></li>
    <li><strong><a name="PARTIESEPL">How are the parties defined in the EPL 1.0?</strong></a><br />
      There are two types of parties to the EPL 1.0. They are
      &quot;Contributors&quot; and &quot;Recipients.&quot; Contributors include
      an initial Contributor, who is the person or entity that creates the
      initial code distributed under the EPL 1.0, and subsequent Contributors,
      who originate changes or additions to the code (the combination referred
      to as the &quot;Program&quot;). Any person or entity that redistributes
      the Program is also a Contributor. Recipients include anyone who receives
      the Program under the EPL, including Contributors.<br /> <br /></li>
    <li><strong><a name="ANONCONTR">Can a Contributor remain anonymous?</strong></a><br />
      No. Except for those who simply redistribute the Program, each Contributor
      must identify itself as the originator of its Contribution in a way that
      later Recipients will be able to readily see.<br /> <br /></li>
    <li><strong><a name="RECRIGHTS">What rights do Contributors grant Recipients
          under EPL?</strong></a><br /> Contributors license Recipients under
      the rights that they have in their Contributions.<br /> <br /></li>
    <li><strong><a name="USEINANOTHER">Does the EPL allow me to take the Source
          Code for a Program licensed under it and include all or part of it in
          another program licensed under the GNU General Public License (GPL),
          Berkeley Software Distribution (BSD) license or other Open Source
          license?</strong></a><br /> No. Only the owner of software can decide
      whether and how to license it to others. Contributors to a Program
      licensed under the EPL understand that source code for the Program will be
      made available under the terms of the EPL. Unless you are the owner of the
      software or have received permission from the owner, you are not
      authorized to apply the terms of another license to the Program by
      including it in a program licensed under another Open Source license.<br />
      <br />
    </li>
    <li><strong><a name="COMPILEWOMOD">Can I take a Program licensed under the
          EPL, compile it without modification, and commercially license the
          result?</strong></a><br /> Yes. You may compile a Program licensed
      under the EPL without modification and commercially license the result in
      accordance with the terms of the EPL.<br /> <br /></li>
    <li><strong><a name="SOURCEWOBJ">Do I need to include the source code for
          such Program with the object code distribution?</strong></a><br /> No.
      But you do need to include a statement that the source code is available
      from you and information on how to obtain it.<br /> <br /> </font>
    </li>
    <li><strong><a name="PROPPROD">When I incorporate a portion of a Program
          licensed under the EPL into my own proprietary product distributed in
          object code form, can I use a single license for the full product, in
          other words, covering the portion of the Program plus my own code?</strong></a><br />
      Yes. The object code for the product may be distributed under a single
      license as long as it references the EPL portion and complies, for that
      portion, with the terms of the EPL.<br /> <br /></li>
    <li><strong><a name="AGREESTEWARD">The EPL states that it can be changed by
          the Agreement Steward. Does a Contributor have the choice of
          redistributing a previously distributed Program under the old or the
          new version of the EPL?</strong></a><br /> While Contributions are
      licensed under the version of the License under which they are originally
      distributed, the EPL provides for the ability of any Contributor to choose
      between that version or a later version.<br /> <br /></li>
    <li><strong><a name="MODNODIST">If I modify a Program licensed under the
          EPL, but never distribute it to anyone else, do I have to make my
          modifications available to others?</strong></a><br /> No. If you do
      not distribute the modified Program, you do not have to make your
      modifications available to others.<br /> <br /></li>
    <li><strong><a name="MODDIST">If I modify a Program licensed under the EPL
          and distribute the object code of the modified Program for free, must
          I make the source code available?</strong></a><br /> Yes. By
      distributing the modified Program, even if it is only a free version of
      the object code, you are obligated to make the source code to the modified
      Program available to others.<br /> <br /></li>
    <li><strong><a name="MODULEDIST">If I write a module to add to a Program
          licensed under the EPL and distribute the object code of the module
          along with the rest of the Program, must I make the source code to my
          module available in accordance with the terms of the EPL?</strong></a><br />
      No, as long as the module is not a derivative work of the Program.<br /> <br /></li>
    <li><strong><a name="SRCREDIST">What are my obligations if I copy source
          code obtained from Eclipse.org and licensed under the Eclipse Public
          License and include it in my product that I then distribute?</a></strong><br />
      Source code licensed under the EPL may only be redistributed under the
      EPL.<br /> <br /></li>
    <li><strong><a name="EPLWARRANTY">Does the EPL offer any warranty with
          regard to the Program?</strong></a><br /> No. The Program released
      under the EPL is provided on an &quot;as is&quot; basis, without
      warranties or conditions of any kind.<br /> <br /></li>
    <li><strong><a name="GETANSWER">This document does not have the answer to my
          question. How can I get my question answered?</a><br /> </strong> You
      may want to check the <a href="/legal/faq">Eclipse Foundation Legal
        Frequently Asked Questions </a>document to see if your question is
      answered there. If not, please send a note to the <a href="mailto:license@eclipse.org">Eclipse Management
        Office</a> and we will do our best to get back to you.<br />
      <br />
    </li>
    <li><strong><a name="DERIV">Some open source software communities specify
          what they mean by a &quot;derivative work&quot;. Does the Eclipse
          Foundation have a position on this?</a><br /></strong> As described in
      article 1(b)(ii) of the Eclipse Public License, &quot;...Contributions do
      not include additions to the Program which: (i) are separate modules of
      software distributed in conjunction with the Program under their own
      license agreement, and (ii) are not derivative works of the Program.&quot;
      The definition of derivative work varies under the copyright laws of
      different jurisdictions. The Eclipse Public License is governed under U.S.
      law. Under the U.S. Copyright Act, a &quot;derivative work&quot; is
      defined as <em>&quot;...a work based upon one or more preexisting works,
        such as a translation, musical arrangement, dramatization,
        fictionalization, motion picture version, sound recording, art
        reproduction, abridgment, condensation, or any other form in which a
        work may be recast, transformed, or adapted. A work consisting of
        editorial revisions, annotations, elaborations, or other modifications
        which, as a whole, represent an original work of authorship, is a
        &quot;derivative work&quot;.&quot;</em> The Eclipse Foundation
      interprets the term &quot;derivative work&quot; in a way that is
      consistent with the definition in the U.S. Copyright Act, as applicable to
      computer software. You will need to seek the advice of your own legal
      counsel in deciding whether your program constitutes a derivative work. <br />
      <br />
    </li>
    <li><strong><a name="LINK">Some free software communities say that linking
          to their code automatically means that your program is a derivative
          work. Is this the position of the Eclipse Foundation?</a><br /></strong>
      No, the Eclipse Foundation interprets the term &quot;derivative work&quot;
      in a way that is consistent with the definition in the U.S. Copyright Act,
      as applicable to computer software. Therefore, linking to Eclipse project
      code might or might not create a derivative work, depending on all of the
      other facts and circumstances. <br /> <br /></li>
    <li><strong><a name="EXAMPLE">I&lsquo;m a programmer not a lawyer, can you
          give me a clear cut example of when something is or is not a
          derivative work?</a><br /></strong> If you have made a copy of
      existing Eclipse code and made a few minor revisions to it, that is a
      derivative work. If you&quot;ve written your own Eclipse Platform Plug-in
      with 100% your own code to implement functionality not currently in
      Eclipse, then it is not a derivative work. Scenarios between those two
      extremes will require you to seek the advice of your own legal counsel in
      deciding whether your program constitutes a derivative work.<br /> <br />
      For clarity, merely interfacing or interoperating with Eclipse Platform
      Plug-in APIs (without modification) does not make an Eclipse Platform
      Plug-in a derivative work.<br /> <br /></li>
    <li><strong><a name="USEEPL">I am starting my own open source software
          project. Does the Eclipse Foundation allow me to use the EPL for my
          project?</a><br /></strong> Yes. The EPL is an OSI-approved open
      source license and may be used unaltered by projects regardless of where
      they are hosted.<br /> <br /></li>
    <li><strong><a name="CODEGEN">Many Eclipse tools and wizards use code
          templates which are included in the application that is generated. Is
          the code generated by these tools considered a derivative work that
          must be licensed under the EPL?</a><br /></strong> Unfortunately,
      there is no clear answer to this question. To the extent that the code
      generated by a wizard is purely functional in nature and therefore not the
      proper subject matter for copyright protection, it could be argued that it
      is not subject to copyright protection, and therefore is not a derivative
      work. An example of that type of code would include calls to APIs or other
      technical instructions which are dictated by functional or technical
      requirements. Moreover, to the extent the generated code is a very small
      part of the final overall work, there is an argument that such use would
      be di minimus, and the final product or application should not be
      considered to be a derivative work. Finally, to the extent developers who
      use the generated code make many changes and additions to the code, there
      is also an argument that the resultant application is not a derivative
      work. Of course, these are just arguments and not "bright line" tests, and
      therefore each position could be subject to differing viewpoints. The
      Eclipse Foundation cannot take a position on this issue, as it will
      ultimately be a question of the facts and circumstances associated with a
      particular use.<br /> <br /></li>
    <li><strong><a name="3RDPARTY">What licenses are acceptable for third-party
          code redistributed by Eclipse projects?</a><br /></strong> The Eclipse
      Foundation views license compatibility through the lens of enabling
      successful commercial adoption of Eclipse technology in software products
      and services. We wish to create a commercial ecosystem based on the
      redistribution of Eclipse software technologies in commercially licensed
      software products. Determining whether a license for third-party code is
      acceptable often requires the input and advice of The Eclipse
      Foundation&rsquo;s legal advisors.<br /> <br />Please see our <a
        href="http://www.eclipse.org/legal/licenses.php">list of the most common licenses</a> approved for use by
      third-party code
      redistributed by Eclipse Foundation Projects. This list is not exhaustive.
      If you have any questions, please contact <a href="mailto:license@eclipse.org">license@eclipse.org</a>.<br />
      <br />
    </li>
    <li><strong><a name="OSICOMPLIANT">Is an OSI-compliant license a requirement
          for all third-party code redistributed by Eclipse projects?</a><br /></strong>
      The Eclipse Foundation fully supports the Open Source Initiative&rsquo;s
      certification of open source licenses, and the Eclipse Public License is
      certified as such. However, there are licenses for software content which
      meet The Eclipse Foundation&rsquo;s requirements for compatibility with
      the EPL and downstream commercial re-distribution that are not OSI
      certified, and Eclipse projects may make use of such licenses after review
      and approval by the Eclipse Foundation.<br /> <br /> The reverse is also
      true: there are OSI-compliant licenses are not compatible with the EPL or
      do not permit downstream commercial re-distribution. Such licenses are not
      used by Eclipse projects.<br /> <br /></li>
    <li><strong><a name="GPLCOMPATIBLE">Are the Eclipse Public License (EPL) 1.0
          and the General Public License (GPL) compatible?</a><br /></strong>
      The EPL 1.0 and the GPL are not compatible in any combination where the
      result would be considered either: (a) a &quot;derivative work&quot;
      (which The Eclipse Foundation interprets consistent with the definition of
      that term in the U.S. Copyright Act ) or (b) a work &quot;based on&quot;
      the GPL code, as that phrase is used in the <a
        href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html">GPLv2</a>, <a
        href="http://www.fsf.org/licensing/licenses/gpl.html">GPLv3</a>
      or the <a href="http://www.fsf.org/licensing/licenses/gpl-faq.html">GPL
        FAQ</a> as applicable. Further, you may not combine EPL 1.0 and GPL code
      in any scenario where source code under those licenses are both the same
      source code module.<br /> <br /> Based upon the <a href="http://www.fsf.org/licensing/licenses/">position</a> of
      the Free Software Foundation, you may not combine EPL 1.0
      and GPL code in any scenario where linking exists between code made
      available under those licenses. The above applies to both GPL version 2
      and GPL version 3.<br /> <br /></li>
    <li><strong><a name="DUALLIC">For Eclipse projects which are dual-licensed,
          your file headers state that the code is being made available under
          two licenses. For example: "This program and the accompanying
          materials are made available under the terms of the Eclipse Public
          License v1.0 and Eclipse Distribution License v. 1.0 which accompanies
          this distribution." What is meant by the use of the conjunction "and"?</a><br /></strong>
      The code is being made available under both of the licenses. The consumer
      of the code can select which license terms they wish to use, modify and/or
      further distribute the code under.</li>
  </ol>
</div>
