/*!
 * Copyright (c) 2021, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import 'eclipsefdn-solstice-assets/js/astro';

// Widgets
import './src/collaborations/eclipsefdn.rollover-link';
import './src/eclipsefdn-promo-content'

// Other Scripts

import './src/blogs-and-videos';
import './src/research/projects-section';
import eclipsefdnEdition from './src/home/eclipsefdn.edition';
import jobPostings from './src/careers/job-postings';

jobPostings.render();
eclipsefdnEdition.render();
