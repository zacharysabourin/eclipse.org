/**
 * Copyright (c) 2024 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import { getNextPage } from 'eclipsefdn-solstice-assets/js/api/utils';

const trademarksEndpoint = "https://api.eclipse.org/foundation/info/trademarks";

/**
 * A Mapper used to convert a Trademark object to an HTML list item.
 * @param {*} trademark The given trademark to convert to HTML
 * @returns a list item in the following format: `<li>${trademark.name}${trademark.demarcation}</li>`
 */
const tradeMarkToListItemMapper = trademark => {
    return `<li>${trademark.name}${trademark.demarcation}</li>`;
}

/**
 * Paginates over all trademark data at a given path, returning all results.
 * @param {*} path The path to the desired trademark data
 * @returns All trademark data in JSON format
 */
async function fetchPaginatedTrademarkDataAsJson(path) {
    let json = [];
    let response = await fetch(trademarksEndpoint + path + '&pagesize=100');
    json = await response.json();

    let nextPage = getNextPage(response.headers.get('link')); 
    while(nextPage) {
        response = await fetch(nextPage.href);
        json.push(... await response.json());
        nextPage = getNextPage(response.headers.get('link')); 
    }
    return json;
}

/**
 * Fetches all trademarks matching the given categories and types, then converts the items to a list of HTML list items. Returns the generated HTML
 * @param {*} category The given array of category names. Can be empty
 * @param {*} types The given trademark types. Either 'R', 'T', or both
 * @returns An HTML list of trademarks mapped to list items.
 */
async function generateTrademarkListItems(categories, types) {
    let queryParams = '';
    if (types.length > 0) {
        queryParams = '?'.concat(types.map(type => `type=${type}`).join('&'));
    }

    let trademarkJson = [];
    if (categories.length == 0) {
        trademarkJson = await fetchPaginatedTrademarkDataAsJson(queryParams);
    } else {
        for (const category of categories) {
            const categoryPathSegment = category ? `/${category}` : '';
            trademarkJson.push(...await fetchPaginatedTrademarkDataAsJson(categoryPathSegment + queryParams));
        }
    }

    return trademarkJson.sort((tm1, tm2) => tm1.name.localeCompare(tm2.name)).map(tradeMarkToListItemMapper).join('');
}

/**
 * Gets all trademarks for each list on the trademarks page and injects the generated list item HTML.
 */
const listAllTrademarks = async () => {
    const allHTML = await generateTrademarkListItems([], ['R']);
    document.getElementById('registered-tm-list').innerHTML = allHTML;

    const otherHTML = await generateTrademarkListItems(['other', 'simrel'], ['T']);
    document.getElementById('other-tm-list').innerHTML = otherHTML;

    const wgHTML = await generateTrademarkListItems(['wg'], ['T', 'R']);
    document.getElementById('wg-tm-list').innerHTML = wgHTML;

    const specHTML = await generateTrademarkListItems(['specification'], ['T', 'R']);
    document.getElementById('spec-tm-list').innerHTML = specHTML;

    const projectHTML = await generateTrademarkListItems(['project'], ['T', 'R']);
    document.getElementById('projects-tm-list').innerHTML = projectHTML;
}

export default listAllTrademarks();