/**
 * Copyright (c) 2024 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

const Mustache = require('mustache');

const licenseItemTemplate = '<li>{{name}} (<a target="_spdx" href="https://spdx.org/licenses/{{alias}}">{{alias}}</a>)</li>';

const generateLicenseList = async () => {
    const response = await fetch('/legal/licenses/licenses.json');
    const data = await response.json();

    // Add date to page
    const modified = new Date(data['meta']['updated']).toDateString();
    document.getElementById('license-updated').innerHTML = `Updated ${modified}`;

    const approvedLicenses = Object.entries(data['approved']).concat(Object.entries(data['valid']));

    // Sort on license name, not spdx alias
    approvedLicenses.sort((a, b) => a.at(1).toLocaleString().localeCompare(b.at(1).toLocaleString()));

    let licenseHtml = '';
    for (const [key, value] of approvedLicenses) {
        licenseHtml = licenseHtml.concat(Mustache.render(licenseItemTemplate, {
            name: value,
            alias: key
        }));
    }
    document.getElementById('license-list').innerHTML = licenseHtml;
}

export default generateLicenseList();