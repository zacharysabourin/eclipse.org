/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

/**
 * A list of all valid article file names. Used for validation
 */
const validArticles = [
    "Article-API-Use/index.html",
    "Article-Accessibility/accessibility.html",
    "Article-Accessibility/index.html",
    "Article-Accessibility351/index.html",
    "Article-ActivexSupportInSwt/index.html",
    "Article-Adapters/index.html",
    "Article-AddingHelpToRCP/index.html",
    "Article-Authoring-With-Eclipse/index.html",
    "Article-AutomatingDSLEmbeddings/index.html",
    "Article-BIRT-ExtensionTutorial1/index.html",
    "Article-BIRT-ExtensionTutorial2/index.html",
    "Article-BIRTChartEngine/index.html",
    "Article-BranchingWithEclipseAndCVS/article1.html",
    "Article-BranchingWithEclipseAndCVS/article2.html",
    "Article-Branding/branding-your-application.html",
    "Article-BuildYourOwnDSL/index.html",
    "Article-Builders/builders.html",
    "Article-BuildingProjectFacets/tutorial.html",
    "Article-Concurrency/jobs-api.html",
    "Article-CustomDrawingTableAndTreeItems/customDraw.htm",
    "Article-CustomDrawingTableAndTreeItems/index.html",
    "Article-Debugger/how-to.html",
    "Article-Decorators/decorators.html",
    "Article-Defining-Generics-with-UML-Templates/index.html",
    "Article-DynamicCSH/index.html",
    "Article-EMF-Codegen-with-OCL/index.html",
    "Article-EMF-goes-RCP/rcp.html",
    "Article-Eclipse-and-Maven2/index.html",
    "Article-EclipseDbWebapps/2006-04-10/article.html",
    "Article-EclipseDbWebapps/index.html",
    "Article-Field-Editors/field_editors.html",
    "Article-Folding-in-Eclipse-Text-Editors/folding.html",
    "Article-Forms/article.html",
    "Article-Forms33/index.html",
    "Article-FromFrontendToCode-MDSDInPractice/article.html",
    "Article-GEF-Draw2d/GEF-Draw2d.html",
    "Article-GEF-EMF/gef-emf.html",
    "Article-GEF-diagram-editor/shape.html",
    "Article-GEF-diagram-editor/shape_cn.html",
    "Article-GEF-dnd/GEF-dnd.html",
    "Article-GEF-editor/gef-schema-editor.html",
    "Article-How-to-Fix-a-Bug-in-Eclipse/index.html",
    "Article-HowToProcessOCLAbstractSyntaxTrees/index.html",
    "Article-Image-Viewer/Image_viewer.html",
    "Article-Integrating-EMF-GMF-Editors/index.html",
    "Article-Internationalization/how2I18n.html",
    "Article-Introducing-AJDT/article.html",
    "Article-Introducing-GMF/article.html",
    "Article-JET/jet_tutorial1.html",
    "Article-JET2/jet_tutorial2.html",
    "Article-JFaceWizards/wizardArticle.html",
    "Article-JFaceWizards/index.html",
    "Article-Java-launch/launching-java.html",
    "Article-JavaCodeManipulation_AST/index.html",
    "Article-LTK/ltk.html",
    "Article-Launch-Framework/launch.html",
    "Article-Levels-Of-Integration/levels-of-integration.html",
    "Article-MemoryView/index.html",
    "Article-Monitor/monitorArticle.html",
    "Article-Mutatis-mutandis/overlay-pages.html",
    "Article-PDE-Automation/article.html",
    "Article-PDE-does-plugins/PDE-intro.html",
    "Article-PDEJUnitAntAutomation/index.html",
    "Article-Plug-in-architecture/plugin_architecture.html",
    "Article-Plugging-into-SourceForge/sourceforge.html",
    "Article-Preferences/article.html",
    "Article-Progress-Monitors/article.html",
    "Article-Properties-View/properties-view.html",
    "Article-RCP-1/tutorial1.html",
    "Article-RCP-2/tutorial2.html",
    "Article-RCP-3/tutorial3.html",
    "Article-Resource-deltas/resource-deltas.html",
    "Article-SWT-Color-Model/swt-color-model.htm",
    "Article-SWT-DND/DND-in-SWT.html",
    "Article-SWT-Design-1/SWT-Design-1.html",
    "Article-SWT-OpenGL/opengl.html",
    "Article-SWT-Virtual/Virtual-in-SWT.html",
    "Article-SWT-browser-widget/browser.html",
    "Article-SWT-graphics/SWT_graphics.html",
    "Article-SWT-graphics/index_cn.html",
    "Article-SWT-images/graphics-resources.html",
    "Article-SimpleImageEffectsForSWT/index.html",
    "Article-Speak-The-Local-Language/article.html",
    "Article-Swing-SWT-Integration/index.html",
    "Article-TPTP-Profiling-Tool/tptpProfilingArticle.html",
    "Article-TPTPAutomateFunctionalTesting/index.html",
    "Article-TVT/how2TestI18n.html",
    "Article-Tabbed-Properties/tabbed_properties_view.html",
    "Article-Table-viewer/table_viewer.html",
    "Article-TreeViewer/TreeViewerArticle.htm",
    "Article-UI-Guidelines/Index.html",
    "Article-UI-Guidelines/index_cn.html",
    "Article-UI-Workbench/workbench.html",
    "Article-Understanding-Layouts/index.html",
    "Article-Unleashing-the-Power-of-Refactoring/index.html",
    "Article-Update/keeping-up-to-date.html",
    "Article-VE-Custom-Widget/customwidget.html",
    "Article-WTP-Persisting-EMF/persisting.html",
    "Article-Web-services-dev-with-wtp/article.html",
    "Article-Workbench-DND/drag_drop.html",
    "Article-WorkbenchSelections/article.html",
    "Article-action-contribution/index.html",
    "Article-babel-pseudo-translations/article.html",
    "Article-small-cup-of-swt/pocket-PC.html",
    "Whitepaper-Platform-3.1/eclipse-platform-whitepaper.html",
    "swt-design-2/swt-design-2.html",
    "using-perspectives/PerspectiveArticle.html",
    "viewArticle/ViewArticle2.html"
];

/**
 * A list of supported article content-types
 */
const validContentTypes = [
    "text/html; charset=utf-8",
    "text/html"
]

/**
 * Validates the incoming file name.
 * File names are considered valid if they are non-null, non-empty, and included in the list of valid articles.
 * @param {string} fileName The given file name to validate
 * @returns {boolean} True if the fileName is valid, false if not
 */
function isValidArticleFile(fileName) {
    return fileName != null && fileName != '' && validArticles.includes(fileName);
}

/**
 * Validates the article file response using the status code and response content-type.
 * Responses are considered valid if they have a 200OK status code and the content-type is included in the list of valid types.
 * @param {number} statusCode The response status code
 * @param {string} contentType The response content-type header value
 * @returns {boolean} True if a valid response. False if not.
 */
function isValidResponse(statusCode, contentType) {
    return statusCode == 200 && validContentTypes.includes(contentType);
}

/**
 * Redirects the page to https://wiki.eclipse.org/Eclipse_Corner
 * Used in instances where certain article validation conditions are not met.
 */
function redirectToEclipseCorner() {
    window.location.replace("https://wiki.eclipse.org/Eclipse_Corner");
}

/**
 * Fetches the desired article and returns the content as a string if it exists and is the correct content-type, 
 * otherwise returns null.
 * @param {string} articleFile The desired article to fetch
 * @returns {Promise<string | null>} A promise containing the article content as text if it exists
 */
async function fetchArticleContentAsText(articleFile) {
    return await fetch(`/articles/${articleFile}`, {
        method: 'GET',
        mode: 'same-origin',
        headers: {
            'Content-Type': 'text/html'
        }
    }).then(data => {
        if (!isValidResponse(data.status, data.headers.get('Content-Type'))) {
            return null;
        }
        return data.text();
    }).catch(err => {
        console.error(err);
        return null;
    });
}

/**
 * Load an article and injects it into the body.
 * Redirects to https://wiki.eclipse.org/Eclipse_Corner if the article is invalid or if the article response is not text/html and 200OK.
 */
const injectArticleContent = async () => {
    const queryParams = new URLSearchParams(window.location.search);
    const articleFile = queryParams.get("file");

    // Only fetch if file name is valid
    if (!isValidArticleFile(articleFile)) {
        redirectToEclipseCorner();
    } else {
        // Article content being null indicates an error or invalid article content. Redirect
        const rawArticleHTML = await fetchArticleContentAsText(articleFile);
        if (!rawArticleHTML) {
            redirectToEclipseCorner();
        } else {
            const baseDir = articleFile.split('/')[0];

            // Parse article content, making all img src relative to /articles/article/
            const articleContent = new DOMParser().parseFromString(rawArticleHTML, 'text/html').body;
            articleContent.querySelectorAll('img').forEach(img => {
                const imgSrc = img.getAttribute('src');
                img.src = `../${baseDir}/${imgSrc}`;
            });

            // Inject article into page
            document.getElementById('article-content').insertAdjacentElement('afterbegin', articleContent);
        }
    }
}

export default injectArticleContent();