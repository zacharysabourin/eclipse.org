// This script will display text indicating no job postings are available. If
// there are job postings, this script will do nothing.

const render = () => {
  const targetNode = document.getElementById('jazz-hr-widget');
  if (!targetNode) return;
  
  const jobListElement = targetNode.querySelector('#resumator-jobs');
  
  // If the job list element is already on the page, try to add the text if
  // needed.
  if (jobListElement) {
    addNoPostingTextIfNeeded(jobListElement);
    return;
  }

  // Otherwise if the job list element is not yet on the page, observe the
  // child list changes. Wait until the JazzHR widget has rendered and then
  // attempt to add the text.
  const callback = (mutationList, _observer) => {
    const jobListElement = targetNode.querySelector('#resumator-jobs');
    if (!jobListElement) return;

    for (const mutation of mutationList) {
      if (mutation.type !== 'childList') {
        continue;
      }

      addNoPostingTextIfNeeded(jobListElement);
      observer.disconnect();
    }
  }

  const observer = new MutationObserver(callback);
  observer.observe(targetNode, { childList: true });
}

export default { render };

/**
  * Adds text indicating there are no open job postings if required. This
  * function does nothing if it is not required.
  *
  * @param element {HTMLElement} - The parent element of the job list items.
  */
const addNoPostingTextIfNeeded = (element) => {
  if (element.children.length > 0) return;

  element.innerHTML = `<p><em>There are no active job postings at the moment. Please check back later!</em></p>`;
}
