---
title: "Amass"
date: 2016-04-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/amass.png"
tags: ["Modeling","Certification"]
homepage: "https://www.amass-ecsel.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/groups/3807241/"
twitter: "https://twitter.com/AMASSproject"
youtube: "https://www.youtube.com/channel/UCw_D0l5sDgysEphi6tzzDyw"
funding_bodies: ["ecsel"]
eclipse_projects: ["polarsys.opencert"]
project_topic: "Tools / Certification"
summary: "Architecture-driven, Multi-concern and Seamless Assurance and Certification of Cyber-Physical Systems"
---

AMASS (Architecture-driven, Multi-concern and Seamless Assurance and Certification of Cyber-Physical Systems) will create and consolidate the de-facto European-wide open tool platform, ecosystem, and self-sustainable community for assurance and certification of Cyber-Physical Systems (CPS) in the largest industrial vertical markets including automotive, railway, aerospace, space, and energy.


The ultimate goal of AMASS is to lower certification costs for CPS in face of rapidly changing features and market needs. This will be achieved by establishing a novel holistic and reuse-oriented approach for architecture-driven assurance (fully compatible with standards such as AUTOSAR and IMA), multi-concern assurance (for co-analysis and co-assurance of e.g. security and safety aspects), and for seamless interoperability between assurance and engineering activities along with third-party activities (e.g. external assessments and supplier assurance).


This project was running from April 2016 to March 2019."

---

## Consortium

* Tecnalia Research & Innovation - Spain (coordinator)
* Honeywell - Czech Rep.
* Telvent Energia SA -- Spain
* KPIT medini Technologies AG - Germany
* Mälardalen University - Sweden
* Eclipse Foundation Europe - Germany
* Infineon IFX - Germany
* AIT Austrian Institute of Technology GmbH - Austria
* Fondazione Bruno Kessler - Italy
* Intecs - Italy
* Berner & Mattner - Germany
* GMV Aerospace and Defence, S.A.U. - Spain
* RINA - Italy
* Thales Alenia Space- Spain
* Universidad Carlos III de Madrid - Spain
* Rapita Systems - United Kingdom
* The REUSE company- Spain
* OHB Sweden AB - Sweden
* Masaryk University - Czech Rep.
* AVL List GmbH- Austria
* Kompetenzzentrum – Das virtuelle Fahrzeug Forschungsgesellschaft mbH - Austria
* Alliance pour les technologies de l'Informatique - France
* Commisariat à l'Energie Atomique et aux energies alternatives - France
* CLEARSY SAS - France
* ALTEN SVERIGE AKTIEBOLAG - Sweden
* Lange Aviation - Germany
* Thales Italia SpA - Italy
* SP Sveriges Tekniska Forskningsinstitut-  Sweden
* Comentor AB - Sweden
