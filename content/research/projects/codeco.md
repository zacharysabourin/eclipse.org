---
title: "CODECO"
date: 2023-01-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/codeco.png"
tags: ["Cloud Architectures", "Cloud Infrastructures", "Network technologies / Internetworking", "IoT", "6 Cloud Services"]
homepage: "https://he-codeco.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/company/codeco-project/"
twitter: "https://twitter.com/CodecoProject"
youtube: "https://www.youtube.com/watch?v=hWftTcG2v3w&list=PLXX_mIW-3A8t36gHoQmby4uVMlqNp5bJb"
zenodo:  "https://zenodo.org/communities/he-codeco/"
funding_bodies: ["horizonEU"]
eclipse_projects: []
project_topic: "Cloud-Edge"
summary: "Cognitive Decentralised Edge Cloud Orchestration"
---

CODECO is a cognitive, cross-layer and highly adaptive Edge-Cloud management framework with a unique orchestration approach
that provides support for data management and governance decentralised data workflow; dynamic offloading of computation and
computation status; and adaptive networking services (TRL5). In the core of the CODECO framework are privacy preserving
decentralised learning mechanisms to i) reduce latency and power consumption from the far Edge to Cloud; ii) adjust the
computation in real-time to available Edge-Cloud constraints; iii) adjust running services into the needs of the application, the data
sources, the surrounding context; iv) benefit from a flexible networking infrastructure that adapts to the needs of active services; and
v) democratize the technology to a faster market adoption of the toolkit, as well as products and services derived from it.

CODECO proposes the following assets: i) A1: Open, cognitive toolkits and smart Apps, integrating the elastic and advanced concepts
to manage, in a smart and flexible way, containerized applications across Edge and Cloud (dynamic cluster and multi-cluster
environment; ii) A2: A developer-oriented open-source software repository, to be available in an early stage of the project, thus
allowing for early exploitation of initial, advanced results and a better adaptation throughout the project lifetime; iii) A3: Training
tools, to support the development of services based on the CODECO framework; iv) A4: Use-cases across 4 domains (Smart Cities,
Energy, Manufacturing, Smar Buildings), as the basis for experimentation and demonstrations; v) A5: Open Calls and multiple
community events, based on the different use-cases and including the different CODECO stakeholders; vi) A6: CODECO integration
into the large-scale EdgeNet , experimental infrastructure, to assist in the building of experimentation and novel concepts by the
research community.


This project is running from January 2023 - December 2025.

---

## Consortium
* Fortiss GmbH (FOR) Germany (Coordinator)
* INOVA DE (INO) Germany (Project Manager)
* ATOS Spain SA (ATOS) Spain
* INTRACOM SA Telecom Solutions (ICOM) Greece
* ATHENA – Research and Innovation Center (ATH) Greece
* University of Göttingen (UGOE) Germany
* Siemens AG (SIE) Germany
* Intrasoft International SA (INTRA) Luxembourg
* Eclipse Foundation Europe GmbH (ECL) Germany
*  FUNDACIO PRIVADA I2CAT, INTERNET I INNOVACIO DIGITAL A CATALUNYA (I2CAT) Spain
* University of Pireus Research Center (UPRC) Greece
* TELEFONICA INVESTIGACION Y DESARROLLO SA (TID) Spain
* Universidad Politecnica de Madrid (UPM) Spain
* Almende (ALM) Netherlands
* IBM RESEARCH GMBH (IBM) Switzerland
* RedHat (RHT) Israel

