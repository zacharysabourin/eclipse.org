---
title: Eclipse Foundation Website Privacy Policy
author: Mike Milinkovich
keywords:
    - legal
    - foundation
    - guidelines
    - trademarks
related:
  links:
    - text: Legal resources
      url: /legal
    - text: Guide to legal documents
      url: /projects/handbook#legaldoc
---

{{< pages/legal/privacy >}}
