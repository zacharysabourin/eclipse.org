---
title: "Eclipse Foundation Open VSX Registry Frequently Asked Questions (FAQ)"
tags: ["frequently asked questions", "faq", "open vsx", "open vsx registry", "extensions", "theia", "vscodium", "licensing"]
---

Last Updated: 8 May 2024

{{< table_of_contents >}}

## What is Open VSX Registry?

The [Open VSX Registry](https://open-vsx.org/) offers a community driven, fully
open platform for publishing and consuming VS Code extensions. The Registry is
built on [Eclipse Open VSX](https://github.com/eclipse/openvsx), which is an
open source project hosted at the Eclipse Foundation.

Open VSX is integrated into multiple software applications that support VS Code
extensions, including [Eclipse Theia IDE](https://theia-ide.org/). There is
strong demand for a fully open alternative to the Visual Studio Marketplace,
and with hundreds of great extensions already available, it is the intent of
the Eclipse Foundation to enable continued and accelerated growth.

## Eclipse Foundation Management

The Open VSX Registry is managed by the Eclipse Foundation's [Open VSX Working Group](https://open-vsx.org/members). 
Through this management, all stakeholders can benefit from vendor-neutral, open
source operation and governance.

## What do I need to know as a Consumer of Open VSX?

If you are consuming extensions in applications that use the Open VSX Registry
(e.g. in Gitpod, VSCodium or Eclipse Theia IDE) there will be no usage impact.
You can expect a seamless experience while we work to add more extensions,
raise the visibility of extensions, and increase the number of consumers to
ensure the long-term viability and vibrancy of the registry.

## What do I Need to Know as a Publisher to Open VSX?

All publishers are required to:

- Sign the [Eclipse Foundation Open VSX Publisher Agreement](/legal/documents/eclipse-openvsx-publisher-agreement.pdf); and
- Ensure all extensions are published under a license.

## Why do I need to Sign a Publisher Agreement?

The purpose of the Eclipse Foundation Open VSX Publisher Agreement is to
provide a written record that you have the authority to enter into and agree to
the terms outlined in the agreement to publish extensions under a license to
the Open VSX Registry.

## How Do I Sign the Publisher Agreement?

Publishers can sign the Eclipse Foundation Open VSX Publisher Agreement via
their profile page on Open VSX Registry.

The following steps are required in order to sign the agreement:

- Create an account at [eclipse.org](https://eclipse.org) and include your GitHub username in the account information;
- Log in at [open-vsx.org](https://open-vsx.org) by authorising the service with your GitHub account;
- Connect Open VSX with your Eclipse account on the [Open VSX account profile](https://open-vsx.org/user-settings/profile) (click on "Log in with Eclipse"); and
- Access and sign the *Eclipse Foundation Open VSX Publisher Agreement* from the [Open VSX account profile](https://open-vsx.org/user-settings/profile) page.

## What Happens if I do not Sign the Publisher Agreement?

If you choose not to sign the Eclipse Foundation Open VSX Publisher Agreement,
you will not be able to publish extensions.

## Do I Need to Sign the Eclipse Contributor Agreement in order to Publish Open VSX Extensions?

No, there is no requirement to sign the [Eclipse Contributor Agreement](/legal/ECA.php). 
You do, however, need to sign the Eclipse Foundation Open VSX Publisher
Agreement.

## Can I sign the Publisher Agreement on behalf of a Service Account?

Yes, the Publisher Agreement accounts for this. If you are able to sign the
Publisher Agreement based on a service account and related service GitHub ID,
coupled with the ability to enter into the Publisher Agreement (necessary
rights to do so based on the terms of the Publisher Agreement), then you may do
so.

Please note if you wish to publish as an individual at a later time, you will
need to create an individual account against a personal GitHub ID and sign the
Publisher Agreement with that ID as an individual.

## Licensing 

### Does my Extension have to be licensed?

Yes, all extensions must be licensed. Your extension's license should be
expressed by including a license expression in the
[package.json](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#license)
manifest. If you have questions about what license is best for you, we
recommend that you read about open source licenses at the [Open Source Initiative](https://opensource.org/licenses).

### Must I Offer my Extension under an Open Source License?

No, you may use a license that is not recognized as an open source license by
the [Open Source Initiative](https://opensource.org/licenses).

### Must the License I Publish my Extension under Match the Source Code License?

No. You can have one license for your source code, e.g. in your GitHub
repository, and a different license specified in your
[package.json](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#license)
manifest for publishing.

### How do I know what License an Extension is Published under?

The applicable licenses of Open VSX Registry extensions are listed on the
respective Overview page in the registry and are included in the JSON metadata
of each extension. This may be an explicit statement regarding the license or a
link to a separate license file.

## How do I Publish Extensions?

The publishing process is described in the [Open VSX Wiki](https://github.com/eclipse/openvsx/wiki/Publishing-Extensions#how-to-publish-an-extension).

## How do I Unpublish an Extension, or a Specific Version of an Extension?

Please file an [issue](https://github.com/eclipse/open-vsx.org/issues) with the
project and a team member will provide this assistance.

## Are Open VSX Registry Extensions Royalty Free?

The Open VSX Registry does not collect fees to publish or consume extensions.
All extensions in the Open VSX Registry are made available without fee, subject
to the terms of the respective licenses.

## How Much is the Open VSX Account Fee?

There are no fees associated with publishing or consuming content from the Open
VSX Registry.

## How do I Opt Out of Auto-Updates?

Auto-update is a feature of VS Code, Eclipse Theia IDE (or other applications
consuming extensions). Please configure the client application accordingly if
you wish to disable auto-updates of extensions. Open VSX Registry itself does
not provide any auto-update facility.

## How do I get Technical Support?

The Open VSX Registry and the Eclipse Foundation cannot provide support for
extensions that you have installed from the registry. The individual publishers
are responsible for providing support for their own offerings. Most extensions
provide links for their source code repositories and/or to report bugs. Or you
can submit a rating and a review using the 'Write a Review' link on the
extension's 'Ratings & Reviews' page.

Users can ask questions, search for answers, enter issues and feature requests
for the Open VSX Registry itself in the project's GitHub [repository](https://github.com/eclipse/open-vsx.org/issues).

## What do I do about an Extension that Appears to be in Violation of the Publisher's Agreement?

The page for each extension has a 'Report Abuse' `mailto:` for this purpose.
These reports are investigated by the project team. You will get a response
once the investigation completes.

## How do I get help with the Publisher Agreement?

Questions related to the Eclipse Foundation Open VSX Publisher Agreement,
licenses, privacy, etc. should be sent to
[license@eclipse.org](mailto:license@eclipse.org).

