---
title: Copyright Agent
author: Mike Milinkovich
keywords:
  - legal
  - privacy
  - policy
related:
  links:
    - text: Legal resources
      url: /legal
    - text: Guide to legal documents
      url: /projects/handbook#legaldoc
---

We respect the intellectual property rights of others, and require that the people who use the Site do the same.
We also maintain a policy under which the Web Site use privileges of users who are repeat infringers of intellectual property rights are terminated in appropriate circumstances.
If you believe that your work has been copied in a way that constitutes copyright infringement, please forward the following information to Eclipse's Copyright Agent, designated as such pursuant to the Digital Millennium Copyright Act, 17 U.S.C. &#167; 512(c)(2), named below:

- Your address, telephone number, and email address;
- A description of the copyrighted work that you claim has been infringed;
- A description of where the alleged infringing material is located;
- A statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law;
- An electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest; and
- A statement by you, made under penalty of perjury, that the above information in your Notice is accurate and that you are the copyright owner or authorized to act on the copyright owner's behalf.

### Copyright Agent

Eclipse Foundation AISBL  
Attention: License Department  
Rond Point Schuman 11  
Brussels 1040 Belgium  

Phone: [+32 2 808 81 52](tel:+3228088152)  
Email: [license@eclipse.org](mailto:license@eclipse.org)
