---
title: "Eclipse Foundation Legal Resources"
layout: single
keywords:
    - legal
    - documents
    - about
---

Legal resource page for the Eclipse Foundation.

> * [Getting Started](./#GettingStarted)
> * [Agreement and Licenses](./#Agreements)
> * [Privacy and Trademarks](./#Trademarks)
> * [Committer Agreements](./#CommitterAgreements)
> * [Committer Resources](./#Committers)

### Getting Started { #GettingStarted }

* The [Guide to the Legal Documents](/projects/handbook/#legaldoc) provides an in-depth look at the many legal documents related to the Eclipse open source community. This content is of value to committers, contributors, redistributors of Eclipse content, developers of Eclipse-based products and users of Eclipse technologies;
* Unless otherwise noted in the project documentation, the [Eclipse Public License](epl-2.0) (EPL) is the default license for Eclipse Foundation projects (please see the [Eclipse Public License Frequently Asked Questions](/org/documents/epl-2.0/faq.php));
* The [Eclipse Foundation Legal Frequently Asked Questions](faq) answers some of the commonly asked questions about Eclipse.org licensing, contributions, working as a committer, and cryptography;
* [Third Party Content Licenses](licenses) provides a list of licenses that are approved for third party content used by Eclipse projects; or
* For other questions, email: [license@eclipse-foundation.org](mailto:license@eclipse-foundation.org).

### Agreements and Licenses { #Agreements }

* [Web Site Terms of Use:](terms-of-use) By accessing, browsing or using this web site, you acknowledge that you have read, understood, and agree to be bound by the terms and conditions contained in this agreement.
* [Eclipse Public License](epl-2.0) (EPL): The default license for Eclipse Foundation projects.
* [Eclipse Distribution License](/org/documents/edl-v10.php) (EDL): The BSD license used by some Eclipse Foundation projects which implement permissive dual-licensing along with the EPL. Other than for [example code or build scripts](non-code-licenses).
* [Eclipse Contributor Agreement:](eca) If you are contributing code or documentation to Eclipse Foundation projects you are required to complete this agreement.
* [Developer Certificate of Origin:](dco) If you are contributing code or documentation to Eclipse Foundation projects, and using the git signed-off-by mechanism, you are agreeing to this certificate.
* [Copyright Agent:](copyright) Contact information for the Eclipse Foundation's Copyright Agent.
* [Eclipse Foundation Software User Agreement:](epl/notice) By downloading binaries or accessing Eclipse Foundation source code repositories, you acknowledge that you have read, understood, and agree to be bound by the terms and conditions contained in this agreement.
* [Approved Licenses for Non-Code, Example, and Other Content:](non-code-licenses) The Board of Directors has approved the use of certain licenses for specific types of content on eclipse.org.
* [Eclipse Foundation Adoptium Marketplace Publisher Agreement:](documents/eclipse-adoptium-marketplace-publisher-agreement.pdf) The license agreement Adoptium Working Group Members must agree to before publishing links in the Adoptium Marketplace from which publisher makes Java SE distributions in binary form.
* [Eclipse Foundation Open VSX Publisher Agreement:](documents/eclipse-openvsx-publisher-agreement.pdf) The license agreement Eclipse Open VSX Registry publishers must agree to before publishing extensions for VS Code compatible editors on [open-vsx.org](https://open-vsx.org/). You may read the Open VSX Registry FAQ [here](open-vsx-registry-faq/).
* [Eclipse Foundation Specification License:](efsl) The license used by Eclipse Foundation specifications created under the Eclipse Foundation Specification Process.
* [Eclipse Foundation TCK License:](tck) The binary license used by Technology Compatibility Kits to demonstrate compatibility with their corresponding Specifications.
* [Eclipse Foundation Quality Verification Suite License:](eclipse-foundation-quality-verification-suite-license) The license that governs the use and redistribution of the Eclipse Quality Verification Suite (QVS) in binary form.
* [Eclipse Foundation User Group Trademark License Agreement:](user-groups) Guidelines and agreements for user groups that are based on Eclipse Foundation projects and/or working groups.

### Privacy and Trademarks { #Trademarks }

* [Privacy Policy:](privacy) Your privacy is important to us. This statement discloses the information practices for this web site, including what type of information is gathered and tracked, how the information is used, and with whom the information is shared.
* [Eclipse Foundation Trademark Usage Guidelines:](logo-guidelines) Guidelines on permissable use of Eclipse logos and trademarks.
* [Trademark and Domain Name Assignment Agreement:](/legal/trademarks/trademark-transfer-agreement.pdf) Pre-existing projects which move to the Eclipse Foundation will be required to execute this agreement to ensure that the Eclipse Foundation has rights to any trademarks associated with the project name.
* [Eclipse Foundation Trademarks:](trademarks) The list of trademarks claimed by the Eclipse Foundation.
* [Trademark Attributions:](trademarks#attribution) Content on this web site may make reference to trademarks requiring attribution.

### Committer Agreements { #CommitterAgreements }

For Eclipse projects (and the open source world in general), _committers_ are the ones who hold the keys. Committers decide what code goes into the code base, they decide how a project builds, and they ultimately decide what gets delivered to the adopter community. Committer status is assigned following a demonstration of merit (generally a record of high-quality contribution to a project) and a successful [committer election](/projects/handbook#elections-committer).

The specific agreements required depends on the nature of the project. For committers on an open source _software_ project (i.e., most Eclipse Foundation projects), the [traditional agreements](/projects/handbook/#paperwork-documents) are required. For committers on an open source _specification_ project, additional [working group agreements](/projects/handbook/#specifications-agreements) are required.

Committer status is assigned on a project-by-project basis. That is, individuals have committer rights only on those projects for which they hold committer status. For all other projects, they are contributors.

**Our committer provisioning process is automated**: new committers will—following their successful election—be contacted by email to engage in our _agreement workflow_ (also referred to as our [paperwork process](/projects/handbook/#paperwork)), which guides them through signing those agreements that they need. The agreements are provided below for convenience.

* [Member Committer and Contributor Agreement:](documents/employer-consent-agreement-for-specification-projects-v1.0.pdf) The Member Committer and Contributor Agreement (MCCA) is used by organizations that are [members of the Eclipse Foundation](/membership) to cover all of their employees who participate in Eclipse Foundation open source projects as committers and contributors. This agreement allows employees of our member organizations to participate in Eclipse Foundation projects without any additional paperwork.
* [Individual Committer Agreement:](/legal/committer-process/eclipse-individual-committer-agreement.pdf) This agreement is used by committers participating in Eclipse open source projects whose employers are either not [members of the Eclipse Foundation](/membership) or are members that have not signed the Member Committer and Contributor Agreement.
* [Employer Consent Agreement for Eclipse Foundation Specification Projects:](documents/employer-consent-agreement-for-specification-projects-v1.0.pdf) The Employer Consent Agreement for Eclipse Foundation Specification Projects ("Employer Consent Agreement") is to be completed by the employer of a committer who is seeking Individual Committer status on an Eclipse Foundation [Specification Project](/projects/efsp) operating under the purview of an Eclipse Foundation Working Group. Note that this form is only required when the employer is not a participant of the corresponding [working group](/org/workinggroups).
* [New Eclipse Foundation Committer and Contributor Agreements FAQ](/legal/committer-process/re-sign) In 2018, we updated our standard contributor and committer agreements, and all our committers and contributors, as well as those members who have member committer agreements, were required to re-sign their agreement with us. This FAQ is mostly historical at this point, but does contain some generally useful information.

### Resources for Committers { #Committers }

A committer is a person that has been granted commit rights to systems on the Eclipse servers such as Git repositories, web pages, download servers, mailing lists and so forth. In addition to writing project content, committers play a critical role is other aspects of managing an open source project.

* [Eclipse Project Handbook:](/projects/handbook) This Eclipse Project Handbook is a reference guide for working as a committer on an Eclipse open source project.
* [Intellectual Property Due Diligence:](/projects/handbook/#ip) At least in some cases, the Eclipse Foundation's IP Team needs to be engaged to review third-party content, or contributions of project content from non-committers. Committers have an important role to play in this process.
  * [Eclipse Committer Due Diligence Guidelines:](committer-guidelines) This document outlines the issues to be aware of and the processes one should follow when working as a committer on Eclipse projects.
  * [Project Content:](/projects/handbook/#ip-project-content) Project Content is content that is managed by the project team. This includes content that is produced by project committers, along with content that is contributed to the project by outside contributors.
  * [Third-party Content:](/projects/handbook/#ip-third-party) The Eclipse Foundation IP Due Dilience Process defines three different types: Prerequisite, Exempt Prerequisite, and Works With Dependency.
  * [IP Logs:](/projects/handbook/#ip-iplog) An IP Log is a record of the intellectual property contributions to a project. This includes such as a list of all committers, past and present, that have worked on the code and (especially) those who have made contributions to the current code base.
* [Legal Documentation:](/projects/handbook/#legaldoc) This provides templates for `NOTICE`, `CONTRIBUTING`, `LICENSE` and other files that accompany project source code and distributions.
* [Committer Elections:](/projects/handbook/#elections) Contributors may become committers via committer election.
