---
title: "New Eclipse Foundation Committer and Contributor Agreements FAQ"
author: "Sharon Scorbett"
keywords: ["legal", "documents", "about", "faq", "committer", "contributor"]
---

As most are aware, the Eclipse Foundation is now about to start hosting specifications as open source projects. This is very exciting for us, and we think it represents a new opportunity for creating innovative specifications using a vendor neutral process. The first specification projects will be a part of the Jakarta EE initiative, but we expect other specification projects to follow shortly.

As a result, we have updated our standard contributor and committer agreements, and all our committers and contributors, as well as those members who have member committer agreements, will need to re-sign their agreement with us.

What Agreements have Changed?
-----------------------------

1. [Eclipse Contributor Agreement](/legal/eca) (ECA);
2. [Eclipse Individual Committer Agreement](/legal/committer-process/eclipse-individual-committer-agreement.pdf) (ICA); and
3. [Eclipse Member Committer and Contributor Agreement](/legal/committer-process/eclipse-member-committer-agreement.pdf) (MCCA) (Formally _Member Committer Agreement_).

What Changes were made to the Agreements?
-----------------------------------------

The changes to the Agreements are basically:

1. We added terms to explicitly allow code contributions to Eclipse projects to be used as the basis for specifications. As you may know, the Eclipse Foundation has created a new specification process, largely motivated by the migration of the Java EE specifications from Oracle's Java Community Process (JCP) to the Eclipse Foundation.
2. We added terms to the committer agreements which embed what is covered by the Eclipse Contributor Agreement (ECA). So now committers only have to sign or be covered by one document. This reduces the amount of paperwork required from committers, and eliminates the requirement for employees of member companies with an MCCA to individually sign an ECA.

The links below provide redlined PDFs showing the differences between the previous version of the agreements, and the revisions announced on November 5, 2018. We encourage you to review these with your advisors.

1. [Eclipse Contributor Agreement](2018-09-26-final-eclipse-contributor-agreement-redline.pdf) (annotated);
2. [Individual Committer Agreement](2018-10-22-final-individual-committer-agreement-redline.pdf) (annotated); and
3. [Member Committer and Contributor Agreement](2018-11-05-final-member-committer-and-contributor-agreement-redline.pdf) (annotated).

How does this Re-signing Exercise affect me?
--------------------------------------------

If you are an existing Eclipse Committer or Contributor, staff will be reaching out personally to you in the future requesting you update your agreement.

Please note that this effort will take a number of months to complete as this involves hundreds of agreements. We are committed to make this update happen in as smooth a fashion as possible and to have virtually no disruption to the flow of contributions being made. We are hopeful that you will be supportive of the change, and will react quickly and favorably when we do contact you.

Does my employer still need to complete the Eclipse Employer Consent Form?
--------------------------------------------------------------------------

No, the Eclipse Employer Consent Form is no longer a requirement as that obligation has now been incorporated into the Individual Committer Agreement (see Section 1.4 Committer Representations)

What is the difference between an ICA, the MCCA and the ECA?
------------------------------------------------------------

The Eclipse Individual Committer Agreement (ICA) is required by Eclipse Committers who are not employed by an Eclipse Member which has a Member Committer and Contributor Agreement on file.

The Eclipse Member Committer and Contributor Agreement (MCCA) can be signed only by Eclipse Members. This agreement covers all employees/contractors/contributors employed by said Member.

The Eclipse Contributor Agreement (ECA) is required by all other non Eclipse Committers in order to contribute to Eclipse projects.

The Member Committer Agreement is now the Member Committer and Contributor Agreement. Why the change?
-----------------------------------------------------------------------------------------------------

This agreement has been updated to include not only those committers in a Member’s employ, but contributors as well. We leveraged this opportunity to incorporate this feature into the former Member Committer Agreement. We plan on providing quarterly updates to members identifying those covered under this agreement.

Do I need to re-sign the ECA if I’ve already signed the updated ICA?
--------------------------------------------------------------------

By re-signing the Individual Committer Agreement (dated Oct/18), you will no longer be required to sign the Eclipse Contributor Agreement.

How do I know if I am already covered by a Member Committer Agreement?
----------------------------------------------------------------------

Your Eclipse Company Representative should know the answer to this question. If they do not or there is uncertainty, please email [emo-records@eclipse.org](mailto:emo-records@eclipse.org) requesting this information.

How do I Re-sign my Agreement?
------------------------------

You will receive a personal email providing you with instructions on how to complete the revised agreement.

I am a Committer employed by a Member Company - Do I have to do anything?
-------------------------------------------------------------------------

If your employer is an Eclipse Member with a Member Committer Agreement on file, we will be working with those Members to arrange for them to sign the updated agreement. You will receive email communication identifying when your employer has signed the new agreement and will confirm you do not need do anything else.

If your employer is an Eclipse Member which does not have a Member Committer Agreement on file, they will have the opportunity to sign the new agreement. However, at this time, we would ask you to re-sign the Individual Committer Agreement only. Please note the Eclipse Employer Consent Form is no longer a requirement.

How will I know if I Re-signed Successfully?
--------------------------------------------

You will receive a copy of the executed agreement via email. If you re-signed the ECA, when you log into eclipse.accounts.org, the system will indicate you have signed The Eclipse Contributor Agreement version 3.0.0.

Are there any legal obligations requiring me to re-sign?
--------------------------------------------------------

No, there are no legal obligations requiring you to re-sign.

What Happens If I do not Re-sign My Committer Agreement?
--------------------------------------------------------

Unfortunately, Individual Committers who do not re-sign will have their committer accounts revoked at the end of the exercise.

What Happens If I do not Re-sign My Contributor Agreement?
----------------------------------------------------------

Unfortunately, Eclipse Contributors will lose their ability to contribute to Eclipse projects.

How can I opt out of this Re-signing Exercise?
----------------------------------------------

Should you decide to opt out, you must relinquish your committer/contributor status. To opt out, send an email to [emo-records@eclipse.org](mailto:emo-records@eclipse.org).

I believe my Employer Information is incorrect in your systems. What do I do?
-----------------------------------------------------------------------------

Please log into [accounts.eclipse.org](https://accounts.eclipse.org/) and update your employer information. It is your responsibility as a committer to ensure we have accurate employer information and the necessary legal paperwork on file to cover your committer account.

I have questions not listed here, who do I contact?
---------------------------------------------------

Please send an email to [license@eclipse.org](mailto:license@eclipse.org)
