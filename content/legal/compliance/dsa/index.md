---
title: "DSA Compliance Disclosure"
date: "2024-02-12"
lastmod: "2024-02-12"
tags: ["digital services act", "content management", "moderation practices", "algorithm", "policy"]
hide_sidebar: true
container: "container padding-bottom-30"
---

Last Updated: 12 February 2024

This page was created to ensure our adherence to the Digital Services Act
(DSA). It includes information on our content management and moderation
practices across our various online platforms.

{{< table_of_contents >}}

## Semi-Annual Moderation Report

This table provides transparency regarding key statistics for our moderation practices across our online platforms from 17 February 2024 to 1 July 2024.

| Website                    | Total Actions Taken | Content Removed | Content Altered | Reason: Spam | Reason: Violation of ToU | User Notified | Appeals Received | Appeals Upheld |
|----------------------------|---------------------|-----------------|-----------------|--------------|--------------------------|---------------|------------------|----------------|
| Adoptium Marketplace       | 0                   | 0               | 0               | 0            | 0                        | 0             | 0                | 0              |
| Eclipse Bugzilla           | 0                   | 0               | 0               | 0            | 0                        | 0             | 0                | 0              |
| Eclipse Forums             | 7                   | 7               | 0               | 7            | 0                        | 0             | 0                | 0              |
| Eclipse GitLab             | 5                   | 5               | 0               | 5            | 0                        | 0             | 0                | 0              |
| Eclipse Marketplace        | 1                   | 1               | 0               | 0            | 1                        | 1             | 0                | 0              |
| Eclipse Newsroom           | 3                   | 2               | 1               | 1            | 0                        | 0             | 0                | 0              |
| Jakarta Blogs              | 0                   | 0               | 0               | 0            | 0                        | 0             | 0                | 0              |
| Open VSX                   | 6                   | 3               | 0               | 0            | 3                        | 3             | 0                | 0              |
| Eclipse Wiki               | 0                   | 0               | 0               | 0            | 0                        | 0             | 0                | 0              |
| Planet Eclipse             | 0                   | 0               | 0               | 0            | 0                        | 0             | 0                | 0              |

## Moderation Practices & Algorithm Information

The general moderation guidelines for the Eclipse Foundation's web properties
include removing content that:

1. Is off-topic for the conversation or content
2. Include third-party links with misleading labels
3. Unsolicited advertisements
4. AI-generated content or incomprehensible text
5. Contains illegal material or exposes personal risk (e.g., residential
   address)
6. Reveals secrets (e.g. passwords) or infringes on intellectual property
7. Appears to manipulate posting requirements or limits
8. Is believed to be spam or in violation of our policies
9. The content was submitted in a language not supported by the platform. All
   our sites default to English unless the platform explicitly supports
   multiple languages. This is to ensure that the content is accessible to our
   global audience

These guidelines are designed to ensure that all content on Eclipse Foundation
web properties respects user privacy, aligns with our policies and processes,
including our [Terms of Use](https://www.eclipse.org/legal/termsofuse.php),
our [Privacy Policy](https://www.eclipse.org/legal/privacy.php), and our
[Community Code of Conduct](https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php),
and maintains the integrity and quality expected by our community.

To dispute a moderation decision, or to report any content that may violate our
Terms of Use or the Digital Service Act (DSA), please contact
<transparency@eclipse-foundation.org>.

## Adoptium Marketplace

*URL: https://adoptium.net/marketplace/*

The Adoptium Marketplace promotes high-quality, TCK certified and Eclipse
AQAvit verified runtimes for use across the Java ecosystem.

For details on our publication guidelines, please visit our
[Adoptium® Marketplace Publisher Guide](https://adoptium.net/docs/marketplace-guide/)
page and the [Adoptium® Software Products Marketplace Policy](https://adoptium.net/docs/marketplace-policy/)
page.

## Eclipse Bugzilla

*URL: https://bugs.eclipse.org/bugs/*

Eclipse Bugzilla is a bug-tracking and reporting platform for the Eclipse
community. This platform allows users and developers to submit and discuss
issues related to Eclipse projects.

Content moderation is done when users report content that does not adhere to
Eclipse Foundation’s moderation guidelines. A foundation staff member will
review and remove it if necessary.

## Eclipse Forums

*URL: https://www.eclipse.org/forums/*

The Eclipse Forums is an interactive platform where the Eclipse community can
engage in discussions, seek help, share information, and collaborate on various
topics related to Eclipse projects and technologies.

Content moderation is done when users report content that does not adhere to
Eclipse Foundation’s moderation guidelines. A foundation staff member will
review and remove it if necessary.

## Eclipse Foundation GitLab

*URL: https://gitlab.eclipse.org/*

GitLab is a DevOps platform for Git repository management, issue tracking, and
CI/CD for open source projects. This is a self-managed instance of Gitlab
maintained by the Eclipse Foundation.

Content moderation is done when users report content that does not adhere to
Eclipse Foundation’s moderation guidelines. A foundation staff member will
review and remove it if necessary.

## Eclipse Marketplace

*URL: https://marketplace.eclipse.org*

The Eclipse Marketplace is a community-powered marketplace platform for
promoting solutions and services related to Eclipse technologies.

Details on our moderation guidelines and content algorithm can be found on our
[Marketplace Client Content Inclusion Policy](https://marketplace.eclipse.org/content/marketplace-client-content-inclusion-policy)
page.

## Eclipse Newsroom

*URL: https://newsroom.eclipse.org/*

The Eclipse Newsroom is a platform that enables staff and members of the
Eclipse community to submit news, events and resources such as, but not limited
to case studies, white papers and market reports. These submissions are
featured across various websites operated by the Eclipse Foundation

Each submission is reviewed by staff before publication to ensure it adheres to
Eclipse Foundation’s moderation guidelines

## Jakarta Blogs

*URL: https://jakartablogs.ee/*

Jakarta Blog is a website that aggregates blog posts from diverse contributors
in the Jakarta EE community. It serves as a platform where individuals
interested in cloud native Java innovation can share their insights, updates,
and thoughts.

Before a new feed is added to Jakarta Blog, our staff will review the current
content to ensure it follows both the Eclipse Foundation’s moderation
guidelines and the [Jakarta Blog guidelines](https://github.com/jakartaee/jakartablogs.ee?tab=readme-ov-file#guidelines).

## Open VSX Registry

*URL: https://open-vsx.org/*

The Open VSX Registry is a vendor-neutral, open-source platform managed by the
Eclipse Foundation for publishing and downloading VS Code extensions.

All publishers must sign the Eclipse Foundation Open VSX Publisher Agreement
and ensure that their extensions are properly licensed.

Listings on the site are sorted by relevance by default, which is calculated
based on user ratings, download counts, and the publishing date of the
extension. Unverified extensions are assigned a lower relevance score, while
those that are recently updated are given a higher score. This algorithm
ensures that relevant extensions are prioritized for the end users.

## Eclipse Wiki

*URL: https://wiki.eclipse.org/*

The Eclipse Wiki is a collaborative content editing platform for the Eclipse
community.

Content moderation is done when users report content that does not adhere to
Eclipse Foundation’s moderation guidelines. A foundation staff member will
review and remove it if necessary.

## Planet Eclipse

*URL: https://planeteclipse.org/planet/*

Planet Eclipse is a website that aggregates and displays blog posts from
various contributors within the Eclipse community. It serves as a platform
where individuals interested in Eclipse technology can share their insights,
updates, and thoughts.

Before a new feed is added to Planet Eclipse, our staff will review its
existing content to ensure it follows both the Eclipse Foundation’s moderation
guidelines and the [Planet Eclipse guidelines](https://gitlab.eclipse.org/eclipsefdn/it/websites/planeteclipse.org#guidelines).

