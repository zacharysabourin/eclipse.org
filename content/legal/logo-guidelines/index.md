---
title: "Eclipse Foundation Trademark Usage Policy"
author: Mike Milinkovich
hide_page_title: true
keywords:
    - certificate
    - ECA
    - contributor
    - eclipse
---

{{< grid/div class="text-center" >}}

# Eclipse Foundation Trademark Usage Policy

of  
Eclipse Foundation AISBL  
a Belgian incorporated international not-for-profit association  
Rond Point Schuman 11, Brussels 1040 Belgium  
VAT BE / Enterprise number: 0760.624.114  
RLE Brussels (French-speaking Enterprise Court)  

and  

Eclipse.org Foundation, Inc.  
a Delaware incorporated 501(c)6 not-for-profit association  

collectively, the “**Eclipse Group**”

{{</ grid/div >}}

Effective as of May 19, 2021

## Purpose: { #purpose }

This Policy (the “**Policy**”) defines the allowable use of names, marks and logos and related rights (referred to herein collectively as "**Trademarks**") owned by Eclipse.org Foundation, Inc. ("**Eclipse**") (such Trademarks, the “**Eclipse Trademarks**”). The Policy is intended to protect the goodwill associated with Eclipse Trademarks and to promote and improve the consistency of use of Eclipse Trademarks across all elements of the Eclipse Group community and otherwise. This is important, among other reasons, to demonstrate that Eclipse Group open source projects (“Eclipse Projects”) are part of a common community of developers, adopters and users, all associated with the highest levels of quality.

Eclipse owns all Trademarks that are related to the business of the Eclipse Group, including the Trademarks related to Eclipse Projects (“**Eclipse Project Trademarks**”) and working groups and their specifications, if any (“**Eclipse WG Trademarks**”). A list of Eclipse Trademarks and any trademark usage guidelines implemented by Eclipse Foundation Working Groups (each, a “**WG**”) (e.g., Jakarta EE, Sparkplug, etc.) which should be read to supplement this Policy (with conflicts governed by this Policy) can be found at [https://www.eclipse.org/projects/tools/trademarks.php](/projects/tools/trademarks.php).

The Eclipse Trademarks are symbols of the quality and community support that people have come to associate with the Eclipse Group. To ensure that the Eclipse Trademarks continue to serve these functions, we need to keep control over their use. People should be able to tell when something is from Eclipse or one of the Eclipse Projects or working groups. Also, Eclipse Trademarks should not be used in a way which tarnishes the reputation or image of Eclipse technology, Eclipse, the Eclipse Group, its projects, its working groups, or its Members.

As discussed in this Policy, there are also specific requirements applicable to Eclipse Trademarks that are logos (“**Eclipse Logos**”), Eclipse Foundation “branded” Trademarks (some of which are Eclipse Logos) (“**Eclipse Foundation Trademarks**”), Eclipse Project Trademarks (some of which are Eclipse Logos), and Eclipse WG Trademarks (some of which are Eclipse Logos).

As detailed below and except as provided herein, the use of any name, mark, or logo which may create confusion with, or falsely implies an endorsement, affiliation or relationship with Eclipse or the Eclipse Group, or is otherwise in violation of Eclipse’s rights or applicable law is a violation of this Policy.

If you have any questions regarding this Policy, wish to report an infringement or improper use of Eclipse Trademarks, or wish to use an Eclipse Trademark in a way not explicitly provided for in this Policy, then contact us by sending an email to [**license@eclipse.org**](mailto:license@eclipse.org).

We recognize that there may be certain circumstances where a “fair use” of an Eclipse Trademark may deviate from this Policy. Nothing in this Policy should be interpreted to interfere with the right to make such fair uses of Eclipse Trademarks as provided for under applicable law and all of the guidance in this Policy is subject to applicable law.

## General Usage of Eclipse Logos { #general-usage-of-eclipse-logos }

1. These requirements apply generally to the use of Eclipse Logos.
  
2. Eclipse-owned logos and related production specifications are available at [https://www.eclipse.org/org/artwork/](/org/artwork). The use of Eclipse-owned logos should follow these specifications and applicable guidelines. Eclipse logos include logos for 'Eclipse', 'Eclipse Foundation', 'EclipseCon', 'Eclipse Summit', 'Eclipse Foundation Member', 'Built on Eclipse', 'Eclipse Ready', 'Jakarta', 'Jakarta EE and any Eclipse Project Trademarks and Eclipse WG Trademarks which are logos. Eclipse Logos cannot be modified in any way without the advanced written permission (email is permissible) of Eclipse. Any Eclipse Project or working group that wishes to seek such permission may do so by sending an email to <license@eclipse.org>.
  
3. To ensure the consistency of our corporate brand identity, please see our [Brand Guidelines Handbook](/legal/documents/eclipse-foundation-branding-guidelines.pdf). This style guide outlines usage standards to ensure that the “Eclipse Foundation” logo is instantly and consistently recognizable however and wherever it is used by Foundation members and the community.

## Proper Usage of Eclipse Foundation Trademarks { #proper-usage-of-eclipse-foundation-trademarks }

These requirements apply to the use of any Eclipse Foundation Trademarks such as the Eclipse Foundation name and logo and the Eclipse Foundation Member logo. Use of the Eclipse Foundation Trademarks are restricted to identifying the Eclipse Foundation and its members. 'Eclipse' must always be capitalized, except when used as part of an Eclipse logo.

Unless otherwise agreed to in advance in writing (which may be in email form) by Eclipse, the following restrictions apply to the use of Eclipse Foundation Trademarks:

1. All of the guidance set out in Section 2 regarding Eclipse Logos apply to Eclipse Foundation Trademarks that are logos.
  
2. You may not incorporate an Eclipse Foundation Trademark into the name of your company or software product or service name.
  
3. You may not include an Eclipse Foundation Trademark in the name of a publication, certification program, domain name or book.
  
4. Only Eclipse Projects are authorized to develop or maintain software packages that use Eclipse Foundation namespaces such as 'org.eclipse' in their namespace. An important use of an Eclipse Trademark is the 'org.eclipse' string used on all namespaces for Eclipse Projects. This naming convention is used to identify code that has been developed as part of an Eclipse Project.
  
5. The 'Eclipse Foundation Member' Trademark is meant to identify those organizations and individuals that are members of the Eclipse Group. Only current members of the Eclipse Group are authorized to use this Eclipse Trademark.

## Proper Usage of Eclipse Project Trademarks { #proper-usage-of-eclipse-project-trademarks }

1. All of the guidance set out in Section 2 regarding Eclipse Logos apply to Eclipse Project Trademarks that are logos.
  
2. The Eclipse Foundation claims the names and logos of all Eclipse Projects as trademarks.
  
3. While every project manages its own development within the broader [Eclipse Development Process](/projects/dev_process/development_process.php), Eclipse holds the Eclipse Project Trademarks for the benefit of the Eclipse community as a whole. The Eclipse Project Naming Policy allows for Eclipse Projects to have a descriptive name, an optional nickname and an acronym. These names (e.g. Theia), and acronyms (e.g. CDT), can become effective identifiers of certain technologies, and therefore it is important to establish maintain proper usage of such identifies. All identifiers used by Eclipse Projects (including project names, nicknames, logos, and acronyms) are the intellectual property of the Eclipse Foundation, and shall constitute Eclipse Trademarks and must be treated as such.
  
4. For clarity, projects are not required to include 'Eclipse' or use the Eclipse logo in their project logos. (Please see proper notice and attribution below.)
  
5. All Eclipse Projects must be identified as being Eclipse projects. Therefore, when referencing an Eclipse Project, we ask that both the first and the most prominent reference to the project on each page is identified as "Eclipse \[project name\]" (e.g., Eclipse Graphical Modeling Framework (GMF) or in the case of a project name that already includes Eclipse, Eclipse Communication Framework (ECF)). Subsequent references may drop the Eclipse identifier.
  
6. You may not incorporate the name of an Eclipse Project Trademark into the name of your company or software product name. If you have a software product that works with an Eclipse Project, you may only use the following phrases: '<product name> for <Eclipse project name>' or '<product name>, <Eclipse project name> Edition'.
  
7. You are required to request permission to include an Eclipse Project Trademark in the name of a publication, certification program, domain name or book. At a minimum, it will be required that a) the appropriate ™ or ® symbol is associated with the Eclipse Project Trademark and appropriate attribution as discussed below, b) the Eclipse Project name is not concatenated with another word, e.g. TheiaPortal, and c) the usage does not imply or infer that the publication, domain name or book is supported or authorized by Eclipse, the Eclipse Group, or any Eclipse Project. If you wish to seek such permission, you may do so by sending an email, prior to any use of an Eclipse Project Trademark, to [license@eclipse.org](mailto:license@eclipse.org), together with a presentation of the projected use of the Eclipse Project Trademark.

## Proper Usage of Eclipse WG Trademarks { #proper-usage-of-eclipse-wg-trademarks }

All of the guidance set out in Section 2 regarding Eclipse Logos apply to Eclipse WG Trademarks that are logos.

The Eclipse Foundation claims the names and logos of all Eclipse Foundation working groups as trademarks. Some Eclipse Foundation working groups produce specifications. The Eclipse Foundation claims the names and logos of each specification as trademarks.

The identifiers of Eclipse WGs are Eclipse WG Trademarks and the intellectual property of Eclipse and must be identified as such in all uses. (Please see proper notice and attribution below.) The use of Eclipse WG Trademarks is restricted to identifying the applicable WG or their deliverables, which may include specifications, as applicable. No other uses of Eclipse WG Trademarks are permitted unless authorized in advance in writing by Eclipse, or in adherence with any supplemental trademark usage guidelines made available at [https://www.eclipse.org/projects/tools/trademarks.php](/projects/tools/trademarks.php). Any organization wishing to seek such authorization may do so by sending an email to [license@eclipse.org](mailto:license@eclipse.org).

Each WG may, at its discretion, develop guidelines for use of the Eclipse WG Trademarks by third parties, provided such guidelines are consistent with this Policy. These WG-specific guidelines will be made available at [https://www.eclipse.org/projects/tools/trademarks.php](/projects/tools/trademarks.php).

One is not permitted to create any trademarks which may create confusion with an Eclipse WG Trademark or suggest endorsement, association or affiliation with a WG or Eclipse Trademark without prior written approval of Eclipse, which, in the event of a request, will consult with the Steering Committee of the relevant Working Group. Any organization wishing to seek permission to create such a Trademark may do so by sending an email to [license@eclipse.org](mailto:license@eclipse.org).

Use of any namespaces created by a WG including but not limited to ‘jakarta’, 'org.locationtech' or 'org.polarsys' is not permitted by this Policy unless authorized in advance in writing by Eclipse.

## Proper Notice and Attribution { #proper-notice-and-attribution }

The appropriate trademark symbol (i.e. ™ or ®) must appear at both the first and most prominent use of Eclipse Trademarks, and with all occurrences of the Eclipse Trademarks.

When you use an Eclipse Trademark you must include a statement attributing the Trademark to the Eclipse Foundation, Inc., by the use of the following sentence:"'\[e.g. Eclipse', 'Built on Eclipse' and 'Eclipse Ready', CDT, Theia\] are trademarks of Eclipse Foundation, Inc."
