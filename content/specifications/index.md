---
title: Specifications
keywords: ["Specifications", "Eclipse Foundation"]
hide_sidebar: true
hide_page_title: true
hide_jumbotron: false
page_css_file : /public/css/specifications-styles.css
container: container-fluid padding-left-0 padding-right-0
header_wrapper_class: header-specifications
jumbotron_class: col-md-24
custom_jumbotron_class: col-md-24
custom_jumbotron: |
    <h1 class="jumbotron-title">Specifications</h1>
    <h2 class="h3 fw-600">
        The Eclipse Foundation Specification Process (EFSP) provides an open and transparent framework for the development of community-driven, open source-friendly specifications.
    </h2>
---

{{< pages/specifications/index >}}
