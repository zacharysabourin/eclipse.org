---
title: "Known Vulnerabilities"
date: 2022-08-30T13:14:48-04:00
description: "List of security vulnerabilities known to affect Eclipse Foundation sites and projects"
keywords: ['Eclipse', 'projects', 'security', 'cve']
---

This page lists security vulnerabilities known to affect Eclipse Foundation sites and projects.

{{< pages/security/known >}}
