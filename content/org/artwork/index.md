---
title: "Eclipse Logos and Artwork"
page_css_file: "/public/css/org/artwork.css"
keywords: ["eclipse", "logo", "eclipse logo", "eclipse logos", "artwork", "logo", "logos", "trademark", "trademarks", "documents", "about"]
author: "Eclipse Foundation"
---

We are providing copies of the Eclipse logo so our community can use it to show their support of Eclipse and link back to our community.
These logos are the intellectual property of the Eclipse Foundation and cannot be altered without Eclipse&apos;s permission. They are provided for use under the [Eclipse Foundation Trademark Usage Policy](https://www.eclipse.org/legal/logo_guidelines.php).

{{< pages/org/artwork/artwork-assets >}}
