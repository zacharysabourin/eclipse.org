---
title: Faster, Easier Setup Accelerates Time to Market
section_id: accelerate-time-to-market
weight: 2
---

It can easily take several years to reinvent all of the governance and processes the Eclipse Foundation has already developed. One of the major European associations that came to the Eclipse Foundation for assistance had been working on its IP policy and bylaws for 18 months with very few tangible results.

In contrast, the Eclipse Software Defined Vehicle Working Group was able to take advantage of the existing Eclipse Foundation framework and processes. As a result, ecosystem members began openly collaborating just three months after initially joining forces. This is an amazing achievement, given the potential complexity that can arise among such a broad range of large, world-class organisations. Founding members include:

{{< pages/collaborations/working-groups/brief/logo_grid >}}