---
title: A Proven Governance and Legal Framework Reduces Risks
section_id: reduce-risks
weight: 1
quote: |
    We have been working hard with Linaro, Seco, Array, NOITechPark, and Synesthesia to prepare Oniro’s initial code contribution and public cloud CI/CD infrastructure, and it is so exciting to see everything moving under the expert governance of the Eclipse Foundation.
quote_author: |
    Davide Ricci,

    Director of Consumer Business Group European Open Source Technology Center, Huawei
---

Associations that attempt to develop their own governance and legal framework for open collaboration likely don’t have the intellectual property (IP) and patent licensing expertise needed to get it right. 

Unfortunately, many associations adopt old-style IP and licensing models that break the open source collaboration model because developers can’t simply take a piece of source code and freely experiment with it. This restriction limits widescale adoption of the open source software, slows technology advancement, and makes the results of the association’s efforts far less relevant. 

Multiple European associations that leverage open source software have approached the Eclipse Foundation for assistance with their governance model. They know the Eclipse Foundation already has an established governance and legal framework for open source collaboration, community building, and innovation. Plus, they see the value in adopting a framework that was built with community support and feedback, and that has been accepted and successfully implemented for years.

Because all Eclipse Foundation members have already agreed to follow the Foundation’s governance framework, there’s no need to work through the many different, and potentially conflicting, interests that can arise when a new association is created.

New open source ecosystems that recognize these benefits, and want to keep governance and legal risks as low as possible, are also choosing to host their efforts at the Eclipse Foundation, including:

- [The Oniro Working Group](https://oniroproject.org/)
- [The Eclipse Software Defined Vehicle Working Group](https://sdv.eclipse.org/)