---
title:
  Contact us about your organisation joining an Eclipse Interest Group
  Collaboration
seo_title: Contact Us | Interest Groups
description: Contact us about your organisation joining an Eclipse Interest Group
keywords: ['interest group', 'join', 'collaboration', 'contact', 'form']
hide_sidebar: true
---

{{< grid/div class="row" isMarkdown="false" >}}
    {{< grid/div class="col-sm-7 margin-top-20 margin-bottom-20" isMarkdown="true" >}}
As per the
[Interest Group Process](/org/collaborations/interest-groups/process.php),
organisations must first be an Eclipse Foundation Member in order to join an
Eclipse Interest Group and must also have a
[Member Committer and Contributor Agreement](/legal/committer_process/EclipseMemberCommitterAgreement.pdf)
executed.
    {{</ grid/div >}}
    {{< grid/div class="solstice-block-default solstice-block-white-bg col-sm-16 col-sm-offset-1 margin-top-20 margin-bottom-20" isMarkdown="false" >}}
        {{< hubspot_contact_form portalId="5413615" formId="a0f7f78b-46f5-459b-ab62-946beae588c3" >}}
    {{</ grid/div >}}
{{</ grid/div >}}
