---
title: "Eclipse Bylaw Changes 2019"
date: 2019-10-24
keywords: ["membership", "vote 2019", "bylaws", "membership agreement", "bylaw changes 2019"] 
---

The voting period ended on December 12, 2019 at 4 pm ET with the new Bylaws
being ratified. The new Bylaws will go into effect on January 1, 2020.

The results of the vote were:

- YES: 186 (96%)
- NO: 7 (4%)

The Eclipse Foundation received approval of its Board of Directors to update
the Eclipse Foundation’s Bylaws at its October 21, 2019 Board meeting.

- [Summary of Bylaws changes](#proposed_changes)
- [Proposed Bylaws](/membership/vote-2019/documents/bylaws_proposed.pdf)
- [Proposed Bylaws annotated](/membership/vote2019/documents/bylaws_proposed_annotated.pdf)
- [2019 Annual Member Meeting Bylaw Presentation (Recorded)](https://youtu.be/Vzw4S1RJd4s)

The current Bylaws of the Eclipse Foundation have served our community well
since 2011. The proposed changes reflect the broader scope in technologies
encompassed by Eclipse projects today as new projects push us into new areas
and working groups turn the Eclipse Foundation into a "large tent of open
collaboration".

In addition to Board approval, Bylaw changes require a vote of the Membership
At-Large. This includes all classes of membership except Associate Members, and
does include Committer Members. The approval of a super-majority (two thirds)
of the Membership At-Large is required for these changes to go into effect.

The election will run through December 12, 2019. Ballots will be distributed by
email to all Company Representatives and Committer Members.

We urge all Members to vote to enable these important changes.

## The proposed changes

- Realign the definition of the Purposes of the Foundation to reflect our
  current activities. This is a key update to recognize the vast portfolio of
  projects, technologies, and specifications hosted by the Foundation
- More clearly state the rights and benefits of affiliate organizations and
  clarify membership dues related to those affiliates, all with the intent to
  make clear affiliate organizations are welcome and encouraged each to
  participate
- Remove the requirement for Solutions Members and higher to release a
  commercial offering, as this requirement has caused significant confusion
  over the years
- Add approval of changes to the Eclipse Foundation Specification Process to
  require a super-majority vote by the Board, thus making it consistent with
  the Eclipse Development Process
- Remove the requirement for a super-majority of the Board to approve changes
  to PMC Leadership (and thus require only a simple majority)
- Clarify the EMO's relationship with its legal counsel
- Realign the quarterly reporting requirements to members to enable the EMO to
  more effectively disseminate information to members
- Clarify and modernize the roles of the Architecture and Planning Councils

## FAQ

#### Q. Why is the Eclipse Foundation making these changes now?

Our Bylaws have not been updated since 2011. At that time, it was a very minor
update. There are several areas of the Bylaws that require clarity and/or
simply updating to reflect present day reality.

#### Q. Why is a vote required to make this change?

The Foundation's Bylaws explicitly state that any change to the Bylaws must be
approved by the Board of Directors as well a vote must be held of the existing
members.

#### Q. Has the Foundation’s Board of Directors approved this change?

Yes, the Board of Directors approved the changes on October 21, 2019 at a face
to face meeting in Ludwigsburg, Germany.

#### Q. Do these changes affect my membership benefits?

No, these changes do not impact or lesson membership rights or have any impact
on our classes of membership.

#### Q. Are my membership fees changing as a result of these changes to the Bylaws?

No, these changes do not impact membership fees.

#### Q. Who is eligible to vote?

All Committer Members and Company Representatives are required to vote.

#### Q. Do all Committers get to vote?

Committer Members are Committers who a) work for a Member Company or b)
individuals who have completed the Committer Membership Agreement.

#### Q. How do I vote?

Ballots will be sent electronically by email communication shortly and will
contain instructions on how to submit your ballot. A simple +1 / 0 / -1 is all
that is required, and the changes are voted on as a whole package. A prompt
response would be greatly appreciated.

#### Q. Why should I bother to vote as this doesn’t affect me or my organization?

These changes cannot be adopted without the approval of our members, and in
order to do that we require a quorum of members to vote. In short, without your
vote, we cannot modernize the Bylaws and make positive change for our members.

#### Q. How does the voting process work?

Each Company Representative vote counts as one vote. Each group of committers
for a Member Company also count as a single vote based on a collapsible single
vote. Each vote by an individual Committer who has completed the Committer
Membership Agreement and is not working for a Member Company also counts as one
vote. The voting process will close end of business December 12, 2019 @ 4pm ET.
A super-majority of affirmative votes is required for the changes to become
effective. The explicit rules for voting, quorum, and eligibility are detailed
in Section 6 of the Eclipse Bylaws<sup>[[1]](#1)</sup>.

#### Q. I do not see any of the benefits of membership noted in the Membership Agreement or Bylaws; how do I find out about those?

Members all have certain rights and obligations as noted in the Bylaws and
Membership Agreement. However, many of the benefits members receive are from
programs run by the Eclipse Foundation - thanks to the support of its
membership. You can find out about these benefits on the membership benefits
page<sup>[[2]](#2)</sup> and some of the special programs being run on the special programs
page. New benefits and programs are always being added and you can find out
about those from your Member Newsletters, Members Meetings, and other
information feeds.

#### Q. Who do I contact for more information?

For email questions, please contact license@eclipse.org. If you would like to
speak to someone directly, please contact Paul White, VP of Member Services,
via 1-613-224-9461 ext. 239.

---

{{< text_target id="1" >}}[1]{{</ text_target >}}
[/org/documents/eclipse_foundation-bylaws.pdf](/org/documents/eclipse_foundation-bylaws.pdf)\

{{< text_target id="2" >}}[2]{{</ text_target >}}
[/membership/](/membership/)

