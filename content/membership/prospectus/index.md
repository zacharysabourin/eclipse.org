---
title: "Membership Prospectus"
date: 2024-03-21
tags: ["membership", "prospectus", "participate", "levels", "strategic", "contributing", "associate", "membership fees", "become member"]
headline: "Eclipse Membership"
subtitle: >-
  Supported by our member organisations, the Eclipse Foundation provides our
  community with Intellectual Property, Mentorship, Marketing, Event and IT
  Services
links: [[href: "/contact", text: "Contact Us About Membership"], [href: "/exploreMembership.php", text: "Explore Our Members"]]
header_wrapper_class: "header-default-bg-img"
container: "container-fluid"
hide_page_title: true
hide_sidebar: true
page_css_file: /public/css/membership-prospectus.css
---

{{< pages/membership/prospectus >}}

