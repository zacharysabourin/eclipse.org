---
title: "Eclipse Membership Changes 2008"
author: "Donald Smith"
date: 2008-07-23
keywords: ["membership", "vote 2008", "bylaws", "membership agreement"] 
---

**Voting concluded on July 23, 2008**. The Membership at large has
overwhelmingly approved the new membership agreement and bylaws and they have
become effective on July 24, 2008. The Eclipse Foundation would like to thank
the Membership for casting their ballots in a timely manner. Here are links to
the new documents:

- [New Bylaws](/org/documents/Eclipse%20BYLAWS%202008_07_24%20Final.pdf)
- [New Membership Agreement](/org/documents/Eclipse%20MEMBERSHIP%20AGMT%202008_07_24%20Final.pdf)

## Final Results

- 95.8% - 157 YES Votes
  > YES, I agree with the unanimous recommendation of the Eclipse Management
  > Organization and Board of Directors to update the Membership Agreement and
  > Bylaws.
- 4.2% - 7 NO Votes
  > NO, I do not support the proposed changes to the Membership Agreement and
  > Bylaws.

Quorum rules required 152 votes for the motion to pass - this was achieved.

## The rest of this page remains for informational / historical purposes

The current [Bylaws of the Eclipse Foundation](/org/documents/eclipse_foundation-bylaws.pdf) 
and [Eclipse Foundation Membership Agreement](/org/documents/Eclipse%20MEMBERSHIP%20AGMT%202008_04_16%20Final.pdf) 
have served our community well for almost five years. To prepare the Eclipse
Ecosystem for the next five years, a number of changes have been proposed to
the Bylaws and Membership Agreement. These changes require super-majority
(two-thirds plus one) approval of the Membership-at-Large to become effective.

On June 23, 2008 we initiated an electronic vote of the Eclipse Foundation
Membership-at-Large to approve the proposed changes. All Member Company
Representatives and Committers should vote on the proposed changes. If you are
a Member Company Represenative or Committer Member and did not receive your
official voting instructions on June 23, 2008 please contact 
<membership@eclipse.org>.

### Benefits of approving the proposed changes

- Allows Eclipse Foundation to continue growing services and support for
  members and committers
  - More members = more and better services
- More opportunities for members and projects
  - Connections into new markets grows overall ecosystem
  - More enterprise use of projects makes them stronger, creates more
    opportunities for committers
- Protects health of ecosystem
  - More connections to more markets insulates ecosystem from some market
    conditions

### Executive Summary of proposed changes

- Create a new membership class - "Enterprise Member"
- Update "Add-In Provider" membership with a more appropriate name - "Solutions
  Member"
- Fix errors and inconsistencies in the current Bylaws

Here is a summary of the proposed Membership Model:

{{< figure class="margin-bottom-40" src="./images/proposed-membership-model.gif" alt="Eclipse Foundation Organizational Membership 2009. Three columns from left to right: Strategic Members contains Strategic Developers, and Strategic Consumers; Sustaining Members contains Enterprise Members and Solutions Members; Supporting Members contains Associate Members and Committer Members." >}}

### Details of proposed changes

You have **three** ways to learn more about the membership changes.

1. A PDF slide-format overview with background and details of the changes can
   be found on the [Membership Changes page](/membership/vote2008/v080624%20-%20PROPOSED%20MEMBERSHIP.pdf).
2. A simple factual summary of the changes:
  - A new membership class called "Enterprise Members" will be created with
    annual dues of $125,000 USD.
  - The "Add-In Provider" membership class will be renamed to "Solutions
    Member."
  - Enterprise and Solutions Members will be referred collectively as
    "Sustaining Members."
  - Sustaining Members will vote for board representation following the same
    formulae used since the Foundation's inception for Add-In Provider board
    representation.
  - A chronological error in the current Bylaws referring to a deadline of
    "Action Without Meeting" is corrected.
  - "Quorum" requirements for future changes to the Membership Agreement and
    Bylaws have been made consistent with both methods of doing so (with
    meeting and without meeting) to be fifty percent plus one. Note that the
    high bar of super-majority affirmative votes is maintained.
  - A logical error referring to a "simple majority" method of collapsing of
    the ballots cast by Committers who work for the same Member Company is
    corrected.
  - The calculation of election properties (number and term of board
    representatives) will be based on a calendar date rather than the moving
    date of the general meeting at EclipseCon.
3. Legal-ese red-lined versions of the proposed new documents are here:
  - [Proposed New Membership Agreement](/membership/vote2008/agreementFINALDRAFT08.pdf) (PDF)
  - [Proposed New Bylaws](/membership/vote2008/bylawsFINALDRAFT08.pdf) (PDF)

### Other recent changes

The Board of Directors has recently made the following changes to "Exhibit C"
of the Membership Agreement. These changes are made by the Board of Directors
and are **not** part of the voting process:

- Any organization may become an Associate member with dues of $5,000 per year.
  Associate membership is still free for Media, Academia, Government,
  Not-for-profits and other organization types as defined by the board.
- Strategic Developer Minimum Dues have been raised by $25,000.
- Add-In Provider Annual Dues for memberships (both new and renewals) on or
  after July 1, 2008 is tiered based on Corporate Revenue (all values USD):
  - (NEW IN 2010) Annual Corporate Revenue Less Than $1 million, and less than
    10 employees and contractors - Fee: 1,500
  - Annual Corporate Revenue Less Than $10 million - Fee: 5,000
  - Annual Corporate Revenue Less Than $50 million - Fee: 7,500
  - Annual Corporate Revenue Less Than $100 million - Fee: 10,000
  - Annual Corporate Revenue Less Than $250 million - Fee: 15,000
  - Annual Corporate Revenue Greater Than $250 million - Fee: 20,000

### FAQ

**Who do I contact for more information?** For email questions, please contact
<membership@eclipse.org>. If you would like to speak to someone in North
America or APAC, please call Donald Smith at +1 (613) 224-9461 x231. In Europe
and India, please contact Ralph Müller at +49 (177) 449-0460.

**How does the voting process work?** All Committer Members and Company
Representatives are required to vote. Ballots have been sent electronically by
email. Committer Members are Committers who work for a Member Company or have
completed the Committer Membership paperwork. Each Company Representative vote
counts as one vote. Each group of committers for a Member Company also count as
a single vote based on a collapsable single vote. Each vote by a Committer who
has completed the Committer Membership paperwork and is not working for a
Member Company will also count as one vote. The voting process will be open for
30 days until close of business July 23, 2008. A super-majority of affirmative
votes is required for the changes to become effective.

**Where do I find more details about the new "Enterprise Member" class?** You
can find details in the slide-show overview on the [membership changes](/go/VOTE2008.PDF) 
page. If you would like more information, please contact
<membership@eclipse.org>.

**I do not see any of the benefits of membership noted in the Membership
Agreement or Bylaws; how do I find out about those?** Members all have certain
rights and obligations as noted in the Bylaws and Membership Agreement.
However, many of the benefits members receive are from programs run by the
Eclipse Foundation - thanks to the support of its membership. You can find out
about these benefits on the [membership benefits](/membership/#tab-membership) 
page and some of the special programs being run on the [special programs](/membership/special-programs) 
page. New benefits and programs are always being added and you can find out
about those from your Member Newsletters, Members Meetings, and other
information feeds.
