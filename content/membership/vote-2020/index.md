---
title: "Eclipse Bylaw and Membership Agreement Changes 2020"
date: 2020-08-28 
keywords: ["membership", "vote 2020", "bylaws", "membership agreement", "bylaw changes 2019"] 
---

The voting period ended on September 29, 2020 with the modifications to the
Eclipse Foundation, Inc.’s Bylaws and Membership Agreement being ratified by a
97% majority. These new Bylaws and Membership Agreement are effective as of
October 1, 2020, and are available on our [Governance Documents](/org/documents/) 
page.

The results of the vote were:

- YES: (97%)
- NO: (3%)

The Eclipse Foundation, Inc. received approval of its Board of Directors to
update the Eclipse Foundation, Inc.'s Bylaws and Membership Agreement at its
August 19, 2020, meeting.

- [Summary of EF-US Bylaws changes](#proposed-bylaw)
- [Revised EF-US Bylaws - clean version](/membership/vote-2020/documents/bylaws_proposed.pdf)
- [Revised EF-US Bylaws annotated](/membership/vote-2020/documents/bylaws_proposed_annotated.pdf)
- [Summary of EF-US Membership changes](#proposed-membership)
- [Revised EF-US Membership - clean version](/membership/vote-2020/documents/membership_proposed.pdf)
- [Revised EF-US Membership Agreement annotated](/membership/vote-2020/documents/membership_proposed_annotated.pdf)

## Background

On May 12, 2020, the Eclipse Foundation announced its transition to Europe as
part of its continued global expansion.

As part of that transition, all incorporation and related documents have been
filed with the Belgian authorities and the Eclipse Foundation awaits the Royal
Decree to formally create the new entity, the Eclipse Foundation AISBL.  The
Decree is expected to be finalized by early-to-mid October.  In parallel, the
Board of Directors of the Eclipse Foundation, Inc. have approved a series of
changes to the Bylaws and Membership Agreement of the existing Delaware,
USA-based entity, in order to align and support the European transition
ensuring the governance of the US organization aligns with the new Belgian
organization.

In addition to Board approval, changes to the Bylaws and Membership Agreement
require a vote of the Membership At-Large.  This includes all classes of
membership except Associate Members and does include Committer Members.  The
approval of a super-majority (two thirds) of the Membership At-Large is
required for these changes to go into effect.

The membership vote will run through September 29, 2020.  Ballots will be
distributed by email to all Company Representatives and Committer Members on
August 31, 2020. 

We urge all Members to vote to enable these important changes.

## The proposed EF-US Bylaw changes are: {#proposed-bylaw}

- Realign the definition of the Purposes of the Foundation to reflect all
  Eclipse technology to better recognize the vast technologies hosted by the
  Foundation  
- Simplify Membership Classes to Strategic, Contributing (formerly Solutions),
  Associate and Committer
- Add "Strategic Foundation" Membership class to enable the new Belgian
  organization to become a member of the Delaware-based organization
- Clarify Member qualifications with respect to Affiliated organizations,
  notably to clarify that affiliated organizations may participate as a single
  Member should they choose to
- Change quorum requirements to reduce the quorum for votes of the
  membership-at-large to 5% for normal decisions and to 33% for major decisions
- Clarify the rights of Board members to have a representative vote at Board
  meetings on their behalf
- Clarify the nomination process for the Board to nominate members to its
  Compensation Committee 
- Clarify that the removal of a Board member "for cause" requires a majority
  vote of the Membership At-Large

## The proposed EF-US Membership Agreement changes are:

- Simplify membership classes to Strategic, Contributing (formerly Solutions),
  Associate and Committer.
- Add Strategic Foundation Membership class
- Clarify use of member name in addition to logo use
- Clarify the requirement of Strategic Members to have two full-time equivalent
  developers engaged in Eclipse projects

## FAQ

**Q. Why is the Eclipse Foundation making these changes now?**

In order to accommodate the transition to Europe, both the Bylaws and the
Membership Agreement for the existing US organization are being modified.  At a
high level, the changes being made are to keep the two organizations in
alignment with respect to classes of membership, membership fees, etc., and to
ensure the governance of the US organization aligns with the new Belgian
organization.

**Q. Why is a vote required to make this change?**

The Foundation’s Bylaws explicitly state that any change to the Bylaws and/or
Membership Agreement must be approved by the Board of Directors as well a vote
must be held of the existing members.

**Q. Has the Foundation’s Board of Directors approved this change?**

Yes, the Board of Directors approved these changes on August 19, 2020.

**Q.  Why did you make changes to the Membership Classes?**

As part of the transition, it was a great opportunity to simplify and clarify
our membership classes, and to correspondingly simplify and shorten the
membership agreement.

**Q. Why was a new membership level of Strategic Foundation added?**

A new class of membership called Strategic Foundation member has been
introduced into the bylaws of the existing US organization. By doing this, the
new Belgian foundation will be an active member of the US organization, and
will help participate in its governance by having seats on its Board. We would
describe this as an “implementation detail”, and would be happy to provide more
detail should you have questions about it.

**Q. Why did we change Solutions membership to Contributing membership? Are there other changes to the classes of membership?**

We are changing the name of the Solutions membership level to Contributing
membership to better reflect the current, diverse group of organizations that
make up our membership base. These organizations participate and contribute to
the development of the Eclipse ecosystem in many ways, including, but not
limited to, leading and contributing to Eclipse Foundation projects, and
offering products and services based on Eclipse technologies.

The Strategic, Committer, and Associate membership classes all remain. For completeness, we have also eliminated the Strategic Consumer and Enterprise classes of membership.  These have not been used for many years, and do not reflect how our members participate.

**Q. Are my membership fees changing as a result of these changes to the Bylaws?**

The Board of Directors has already approved the fee changes that come into
effect October 1, 2020 for both the Beligan-based Eclipse Foundation AISBL and
the existing Eclipse Foundation, Inc. The fees are now stated in Euros while
retaining the same numeric value, e.g. if you are currently paying $20,000 the
dues in the new organization will be 20.000€. However, we do understand this
represents a dues increase, and to help mitigate the increase, all member
renewal fees between October 1, 2020 and September 30, 2021 will be discounted
by 10%. So, to continue this example, if your membership fee in 2020 was
$20,000, your fee will be 18.000€ in 2021, and 20.000€ in 2022 and beyond. 

This modest fee increase is the first in almost 15 years - we hope that members
appreciate the merit of stating all fees in Euros as we complete this
transition.

Note that working group fees are not changing as a result of the move, and will
remain in USD.  Each working group’s Steering Committee, as part of its
responsibilities, will evaluate whether they should change their fees to be
stated in euros and/or to adjust the fees based on such a change.  Should the
working group choose to take this action, any change will be communicated in
advance of enacting the change.

If you are an individual Committer Member you will be asked to execute the new
Belgian membership agreement. There remain no fees for Committer membership.

More information on executing new agreements will follow in the October
timeframe.

**Q. When do these changes take effect, and what steps do I need to take?**

Once the new Belgian based organization is created, expected in early-to-mid
October, we will be reaching out to all members to engage with them in
supporting this transition.  Specifically, we will be asking members to
re-execute each of the agreements listed above that your organization has
executed previously.  We will help you with this process by providing you
copies of your existing agreements, and the new ones to be executed.

Concurrent with executing these new versions of the agreements, we will be
asking you to execute a letter confirming the resignation of your membership in
the existing US-based organization.  This formality is required, as that entity
will exist for some period of time, and we want to avoid having any
organizations paying fees to both the current and the new foundations.

**Q. Who is eligible to vote for the changes to the Bylaws and Membership Agreement?** 

All Committer Members and the Company Representatives of all Strategic and
Solutions members are eligible to vote.

**Q. Do all Committers get to vote?** 

Committer Members are Committers who either a) work for a Member Company or b)
individuals who have completed the Committer Membership Agreement. Only
Committers that are Committer Members are eligible to vote.

**Q. How do I vote?**

Ballots will be sent electronically by email communication shortly and will
contain instructions on how to submit your ballot. A simple yes or no is all
that is required, and the changes are voted on as a whole package. A prompt
response would be greatly appreciated.

**Q. Why should I bother to vote?**

These changes cannot be adopted without the approval of our members, and in
order to do that we require a quorum of members to vote.  It's extremely
important to recognize that your vote is required to enact the changes
necessary to stand up the Belgian organization.

**Q. How does the voting process work?**

Each Company Representative of a Strategic or Solutions member’s vote counts as
one vote. Each group of committers for a Member company also count as a single
vote based on a collapsible single vote. Each vote by an individual Committer
who has completed the Committer Membership Agreement and is not working for a
Member Company also counts as one vote. The voting process will close at the
end of business September 29, 2020 @ 4pm ET. A super-majority of affirmative
votes is required for the changes to become effective. The explicit rules for
voting, quorum, and eligibility are detailed in Section 6 of the Eclipse
Bylaws[[1]](#1). 

**Q. I do not see any of the benefits of membership noted in the Membership Agreement or Bylaws; how do I find out about those?** 

Members all have certain rights and obligations as noted in the Bylaws and
Membership Agreement. However, many of the benefits members receive are from
programs run by the Eclipse Foundation - thanks to the support of its
membership. You can find out about these benefits on the membership benefits
page[[2]](#2) and some of the special programs being run on the special programs
page. New benefits and programs are always being added and you can find out
about those from your Member Newsletters, Members Meetings, and other
information feeds.

**Q.  I understand there is more information generally available as part of the Eclipse Foundation Inc.’s move to Europe?**

Yes, the Eclipse Foundation has prepared an in-depth Membership FAQ regarding
its transition to Europe. Additionally the Eclipse Foundation recently held a
Town Hall on the matter. You may view the presentation materials here. 

**Q. Who do I contact for more information?**

For email questions or to schedule a time to speak to someone directly, please
contact <eclipse-europe@eclipse.org>.

---

{{< text_target id="1" >}}[1]{{</ text_target >}} 
[https://www.eclipse.org/org/documents/eclipse_foundation-bylaws.pdf](/org/documents/eclipse_foundation-bylaws.pdf)\

{{< text_target id="2" >}}[2]{{</ text_target >}} 
[https://www.eclipse.org/membership/](/membership/)
